// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2022   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/helpwelcome.dart';
import 'package:MigraineLog/config.dart';
import 'package:MigraineLog/editor.dart';
import 'package:MigraineLog/viewer.dart';
import 'package:MigraineLog/stats.dart';
import 'package:platform/platform.dart';
import 'utils.dart';
import 'main_mocks.dart';

void main() {
  var skipGoldenTests = !LocalPlatform().isLinux;

  TestWidgetsFlutterBinding.ensureInitialized();
  setTestFile('main_full');
  packageInfoMock();

  group('MigraineLog', () {
    testWidgets('golden stock', (WidgetTester tester) async {
      await tester.setScreenSize(width: 540, height: 760);
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await goldenTest(
        widgetType: FakedMigraineLog,
        name: 'MigraineLog',
        tester: tester,
      );
    }, skip: skipGoldenTests);
    testWidgets('Config', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.more_vert), findsOneWidget);
      expect(find.text('Settings'), findsNothing);

      await tester.tap(find.byIcon(Icons.more_vert));
      await tester.pumpAndSettle();

      expect(find.text('Settings'), findsOneWidget);
      await tester.tap(find.text('Settings'));
      await tester.pumpAndSettle();

      expect(find.byType(MigraineLogConfigScreen), findsOneWidget);
    }, tags: ['skip-test-coverage']);
    testWidgets('About dialog', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.more_vert), findsOneWidget);
      expect(find.text('About'), findsNothing);

      await tester.tap(find.byIcon(Icons.more_vert));
      await tester.pumpAndSettle();
      expect(find.text('About'), findsOneWidget);
      await tester.tap(find.text('About'));
      await tester.pumpAndSettle();
      expect(find.byType(AboutDialog), findsOneWidget);
    }, tags: ['skip-test-coverage']);
    testWidgets('Add', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.add), findsOneWidget);

      await tester.tap(find.byIcon(Icons.add));
      await tester.pumpAndSettle();

      expect(find.byType(MigraineLogEditor), findsOneWidget);
    }, tags: ['skip-test-coverage']);
    testWidgets('Help', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.more_vert), findsOneWidget);
      expect(find.text('Help'), findsNothing);

      await tester.tap(find.byIcon(Icons.more_vert));
      await tester.pumpAndSettle();

      expect(find.text('Help'), findsOneWidget);
      await tester.tap(find.text('Help'));
      await tester.pumpAndSettle();

      expect(find.byType(MigraineLogHelp), findsOneWidget);
    }, tags: ['skip-test-coverage']);
    testWidgets('Calendar tab', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogViewer), findsNothing);
      expect(find.byIcon(Icons.calendar_today), findsOneWidget);
      await tester.tap(find.byIcon(Icons.calendar_today));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogViewer), findsOneWidget);
      await tester.flush();
    }, tags: ['skip-test-coverage']);
    testWidgets('Stats tab', (WidgetTester tester) async {
      await tester.pumpWidget(
        FakedMigraineLog(),
      );
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogStatsViewer), findsNothing);
      expect(find.byIcon(Icons.pie_chart), findsOneWidget);
      await tester.tap(find.byIcon(Icons.pie_chart));
      await tester.pumpAndSettle();
      expect(find.byType(MigraineLogStatsViewer), findsOneWidget);
      await tester.flush();
    }, tags: ['skip-test-coverage']);
  });
}
