// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You must have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';
import 'dart:io';
import 'package:MigraineLog/localehack.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:path/path.dart' as p;

void main() {
  // The working directory differs from when we run the test explicitly with
  // 'flutter test ./test/i18n_test.dart' and when we run all tests with
  // 'flutter test', so try to detect where we should look for the i18n dir. By
  // default assume ./i18n, otherwise try ../i18n.
  var root = './';
  if (!Directory(root + 'i18n').existsSync()) {
    root = '../';
  }
  var dir = Directory(root + 'i18n/');
  var source =
      jsonDecode(File(root + 'i18n/intl_messages.arb').readAsStringSync());
  // Iterate over translation entries
  for (var entry in dir.listSync()) {
    var path = entry.path;
    // Skip nything that's not a File
    if (entry is! File) {
      continue;
    }
    // Skip intl_messages.arb and any non-ARB files (also skips temporary
    // files, like lang.arb~)
    if (path == root + 'i18n/intl_messages.arb' ||
        path == root + 'i18n/en.arb' ||
        !path.endsWith('.arb')) {
      continue;
    }
    // Validate the i18n file
    test("i18n validation: $entry", () async {
      String content = await entry.readAsString();
      var i18nContent = jsonDecode(content);
      expect(i18nContent, TypeMatcher<Map>(),
          reason: 'Decoding the ARB-file must result in a map');
      for (String entry in source.keys) {
        expect(i18nContent[entry], isNotNull,
            reason:
                'All entries in the source file must be in the language file');
        if (source[entry] is! String) {
          continue;
        }
        for (var word in source[entry].split(RegExp(r"\s+"))) {
          if (!word.startsWith('{')) {
            continue;
          }
          var placeholderEndIdx = word.indexOf("}");
          if (placeholderEndIdx == -1) {
            continue;
          }
          word = word.substring(0, placeholderEndIdx);
          expect(i18nContent[entry], contains(word),
              reason:
                  'All placeholders must be present in the translated text (entry ' +
                      entry +
                      ')');
        }
        if (entry ==
            '{number,plural, =1{{number} day}=2{{number} days}other{{number} days}}') {
          expect(i18nContent[entry],
              containsAllInOrder(['{number}', '{number}', '{number}']));
          // If we have another entry than the above that defines a plural
          // word, then the test need to be updated, so we shout loudly about
          // it.
        } else if (entry.startsWith('{') && entry.contains(',plural,')) {
          throw ("Test needs updating for new plural entry");
        }
        if (entry != 'for details.') {
          expect(i18nContent[entry], isNot("."),
              reason:
                  "We assume all entries contain some text. If there is a reason why a string should be '.' in your language, open up an issue (https://gitlab.com/mglog/org.zerodogg.migraineLog/-/issues) so that the UI can be modified so that this makes sense (otherwise, while it will work fine, you will end up with weird spacing around your '.')");
        }
        expect(i18nContent[entry], isNot(''));
      }
      expect(i18nContent['TRANSLATED_BY'], isNot("TRANSLATED_BY"),
          reason: 'TRANSLATED_BY must be changed');
      var code = RegExp(r'\/([^/]+)\.arb').firstMatch(path)!.group(1);
      expect(LocaleHack.supportedLocales, contains(code),
          reason:
              "For the locale to be enabled it must be in the 'supportedLocales' array of LocaleHack, located in lib/localehack.dart");
      String? dateLocaleCode = LocaleHack.dateLocaleMap[code!] ?? code;
      expect(dateTimeSymbolMap()[dateLocaleCode], isNotNull,
          reason:
              "For the locale to work properly with dates, it must be in the 'dateLocaleMap' of LocaleHack, located in lib/localehack.dart");
    });
  }
  // Check fastlane
  var fastlane = Directory(root + 'android/fastlane/metadata/android/');
  for (var entry in fastlane.listSync()) {
    if (!FileSystemEntity.isLinkSync(entry.path)) {
      test(p.basename(entry.path), () async {
        var code = p.basename(entry.path);
        var short = File(entry.path + '/short_description.txt');
        var full = File(entry.path + '/full_description.txt');
        var title = File(entry.path + '/title.txt');
        expect(p.basename(entry.path), contains('-'),
            reason:
                'The fastlane directory needs to be in the form LANG-COUNTRY, ie. en-US and en-GB');
        expect(short.existsSync(), isTrue,
            reason: 'short_description.txt is required');
        expect(full.existsSync(), isTrue,
            reason: 'full_description.txt is required');
        expect(title.existsSync(), isTrue, reason: 'title.txt is required');
        expect(short.readAsStringSync().length, lessThanOrEqualTo(80));
        expect(full.readAsStringSync().length, greaterThanOrEqualTo(120));
        expect(full.readAsStringSync().length, lessThanOrEqualTo(4000));
        expect(title.readAsStringSync().length, greaterThanOrEqualTo(5));
        expect(title.readAsStringSync().length, lessThanOrEqualTo(50));
        for (var line in File(root + "/Makefile").readAsLinesSync()) {
          if (line.startsWith("SCREENSHOT_LANGUAGES") && code != "nn") {
            expect(line, contains(code),
                reason:
                    "SCREENSHOT_LANGUAGES in the Makefile should include the language, to ensure we will build screenshots for it");
          }
        }
      });
    }
  }
}
