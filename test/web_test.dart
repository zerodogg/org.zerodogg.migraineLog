// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2024    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

@TestOn('browser')
library;

import 'package:test/test.dart';
import 'package:MigraineLog/web.dart' as mlw;
import 'dart:html';
import 'dart:convert';

Map getTestData() {
  return {
    "exportVersion": 1,
    "version": "testing",
    "data": {
      "202101": {
        "2021-01-01": {
          "strength": 1,
          "note": "Testnote",
          "medicationList": [
            {"name": "Medication", "timeString": "12:00"}
          ],
          "date": "Friday, 1. January 2021"
        },
        "2021-01-05": {
          "strength": 2,
          "note": "Testnote",
          "medicationList": [
            {"name": "Medication", "timeString": "12:00"}
          ],
          "date": "Tuesday, 5. January 2021"
        },
        "2021-01-09": {
          "strength": 3,
          "note": "",
          "medicationList": [],
          "date": "Saturday, 9. January 2021"
        }
      }
    },
    "summaries": {
      "202101": {
        "headacheDays": "3 days",
        "medicationDays": "2 days",
        "specificSummaries": {
          "Strong migraine": "1 day",
          "Migraine": "1 day",
          "Headache": "1 day"
        }
      }
    },
    "strings": {
      "messages": {
        "migraineLog": "Migraine Log",
        "note": "Note",
        "none": "(none)",
        "strength": "Strength",
        "date": "Date",
        "takenMeds": "Medications",
        "totalHeadacheDays": "Total headache days",
        "hide": "hide",
        "hideTooltip": "Hide this month",
        "hidden": "hidden:",
        "months": "months",
        "everything": "everything",
        "show": "show",
        "filter": "Filter:",
        "listEmptyDays": "List days with no entries",
        "dateStrings": {
          "weekdays": [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
          ],
          "months": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
          ]
        }
      },
      "strength": {
        "2": "Migraine",
        "3": "Strong migraine",
        "1": "Headache",
        "-1": "Aura only",
        "-2": "No headache"
      },
      "months": {"202101": "January 2021"}
    }
  };
}

void main() {
  Map testData = getTestData();

  // Ensure old HTML isn't still around before running each test
  setUp(() async {
    querySelector('body')!.innerHtml = '';
    return true;
  });

  group('MigraineLogWebBase (via dataToHTML)', () {
    test('getHumanMonthString', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.getHumanMonthString("202101"), 'January 2021');
      expect(dh.getHumanMonthString("202101"),
          testData["strings"]["months"]["202101"]);
    });
    test('strength', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.strength("1"), testData["strings"]["strength"]["1"] + ' (1)');
      expect(dh.strength("-1"), testData["strings"]["strength"]["-1"]);
      expect(dh.strength("-2"), testData["strings"]["strength"]["-2"]);
    });
    test('message', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.message("note"), testData["strings"]["messages"]["note"]);
    });
    test('formattedDateTime', () {
      var dh = mlw.dataToHTML(data: testData);
      var dt = DateTime(2021, 1, 1);
      expect(dh.formattedDateTime(dt), 'Friday, 1. January 2021');
    });
    test('getMonthWithEmpty', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.getMonthWithEmpty('202101'), TypeMatcher<Map>());
      expect(dh.getMonthWithEmpty("202101").length, 31);
    });
    test('getMonth', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.getMonth('202101'), TypeMatcher<Map>());
      expect(dh.getMonth("202101"), testData["data"]["202101"]);
      expect(dh.getMonth('202101')!.length, 3);
      expect(dh.manager, isNotNull);
      dh.manager!.displayEmptyDays = true;
      expect(dh.getMonth('202101')!.length, 31);
    });
    test('getMonthSummary', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.getMonthSummary("202101"), testData["summaries"]["202101"]);
    });
    test('medicationListToStr', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.medicationListToStr([]), dh.message('none'));
      final basicString = dh.medicationListToStr([
        {"name": "Hello", "timeString": "10:00"}
      ]);
      expect(basicString, contains("Hello"));
      expect(basicString, contains("10:00"));
      final extendedString = dh.medicationListToStr([
        {"name": "Hello", "timeString": "10:00"},
        {"name": "World", "timeString": "12:00"}
      ]);
      expect(extendedString, contains('Hello'));
      expect(extendedString, contains('World'));
      expect(extendedString, contains('10:00'));
      expect(extendedString, contains('12:00'));
    });
  });

  group('dataToHTML', () {
    test('sorted', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.sorted(["a", "b", "c"]), ["c", "b", "a"]);
    });
    test('monthSummary', () {
      var dh = mlw.dataToHTML(data: testData);
      var result = dh.monthSummary("202101");
      expect(result, TypeMatcher<Element>());
      expect(result.children.length, 5);
      expect(result.innerHtml,
          "<tr><td>Strong migraine</td><td>1 day</td></tr><tr><td>Migraine</td><td>1 day</td></tr><tr><td>Headache</td><td>1 day</td></tr><tr><td>Medications</td><td>2 days</td></tr><tr><td>Total headache days</td><td>3 days</td></tr>");
    });
    test('monthToHTML', () {
      var dh = mlw.dataToHTML(data: testData);
      var result = dh.monthToHTML("202101");
      expect(result, TypeMatcher<Element>());
      expect(result.children.length, 3);
      expect(result.innerHtml,
          '<h2>January 2021<span class="hide-month" title="Hide this month">[hide]</span></h2><table><tr><td>Strong migraine</td><td>1 day</td></tr><tr><td>Migraine</td><td>1 day</td></tr><tr><td>Headache</td><td>1 day</td></tr><tr><td>Medications</td><td>2 days</td></tr><tr><td>Total headache days</td><td>3 days</td></tr></table><table class="table"><thead><tr><th>Date<span class="sortArrow"> ▼</span></th><th>Strength<span class="sortArrow" style="visibility: hidden;"> ▼</span></th><th>Medications<span class="sortArrow" style="visibility: hidden;"> ▼</span></th><th>Note<span class="sortArrow" style="visibility: hidden;"> ▼</span></th></tr></thead><tbody><tr><td>Saturday, 9. January 2021</td><td>Strong migraine (3)</td><td>(none)</td><td></td></tr><tr><td>Tuesday, 5. January 2021</td><td>Migraine (2)</td><td>12:00 Medication<br></td><td>Testnote</td></tr><tr><td>Friday, 1. January 2021</td><td>Headache (1)</td><td>12:00 Medication<br></td><td>Testnote</td></tr></tbody></table>');
    });
    group('buildFilterUI', () {
      test('Basic', () {
        var dh = mlw.dataToHTML(data: testData);
        querySelector('body')!.append(dh.buildFilterUI());
        expect(querySelector('input'), isNotNull,
            reason: 'Should always have toggle box to display all days');
        expect(querySelector('select'), isNull,
            reason: 'Should not have a select box with a single month');
      });
      test('Multi-month select', () {
        var privTestData = getTestData();
        // This isn't actually valid data and will crash if used as such, but
        // it's sufficient data for buildFilterUI to work
        for (var i = 0; i < 12; i += 1) {
          privTestData["data"]["2020" + i.toString().padLeft(2, '0')] =
              privTestData["data"]["202101"];
        }
        var dh = mlw.dataToHTML(data: privTestData);
        querySelector('body')!.append(dh.buildFilterUI());
        expect(querySelector('select'), isNotNull);
        // 13(months)/3(permits limiting every 3 months)=4.333=~4
        // total=4+3 (for the "show all" and 4+5)=7
        expect(querySelectorAll('option').length, 7);
      });
      test('Toggle emptyDaysCheckbox', () async {
        var dh = mlw.dataToHTML(data: testData);
        querySelector('body')!.append(dh.buildFilterUI());
        expect(querySelector('#emptyDaysCheckbox'), isNotNull);
        expect(dh.manager!.displayEmptyDays, isFalse);
        expect((querySelector('#emptyDaysCheckbox') as InputElement).checked,
            isFalse);

        querySelector('#emptyDaysCheckbox')!.click();
        expect((querySelector('#emptyDaysCheckbox') as InputElement).checked,
            isTrue);
        expect(dh.manager!.displayEmptyDays, isTrue);

        querySelector('#emptyDaysCheckbox')!.click();
        expect(dh.manager!.displayEmptyDays, isFalse);
      });
    });
  });
  group('MonthTableManager', () {
    test('defaults', () {
      var m = mlw.MonthTableManager();
      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.desc);
      expect(m.displayEmptyDays, isFalse);
    });
    test('listen/set', () {
      var called = false;
      var m = mlw.MonthTableManager();
      m.listen(() {
        called = true;
      });
      expect(called, isFalse);
      m.set(mlw.sortField.date, mlw.sortDirection.asc);
      expect(called, isTrue);
    });
    test('displayEmptyDays', () {
      var m = mlw.MonthTableManager();
      expect(m.displayEmptyDays, isFalse);
      m.displayEmptyDays = true;
      expect(m.displayEmptyDays, isTrue);
    });
  });
  group('MonthTable', () {
    test('element', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.element, TypeMatcher<Element>());
    });
    test('getField', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.getField({}, 'test'), '');
      expect(e.getField({'test': 'test'}, 'test'), TypeMatcher<String>());
      expect(e.getField({'test': 'test'}, 'test'), 'test');
      expect(e.getField({'test': 1}, 'test'), TypeMatcher<int>());
      expect(e.getField({'test': 1}, 'test'), 1);
      expect(
          e.getField({
            'test': ['hello', 'world']
          }, 'test'),
          TypeMatcher<String>());
      expect(
          e.getField({
            'test': ['hello', 'world']
          }, 'test'),
          'helloworld');
    });
    test('sortingArrow', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.sortingArrow(mlw.sortField.strength), TypeMatcher<Element>());
      expect(e.sortingArrow(mlw.sortField.strength).innerHtml, contains('▼'));
      expect(e.sortingArrow(mlw.sortField.strength).outerHtml,
          contains('sortArrow'));
      expect(e.sortingArrow(mlw.sortField.strength).outerHtml,
          contains('visibility: hidden'));

      expect(e.sortingArrow(mlw.sortField.date).outerHtml,
          isNot(contains('visibility: hidden')));

      m.set(mlw.sortField.strength, mlw.sortDirection.asc);
      expect(e.sortingArrow(mlw.sortField.strength).innerHtml, contains('▲'));
    });
    test('tableHeader', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.tableHeader("note", mlw.sortField.note), TypeMatcher<Element>());
      expect(e.tableHeader("note", mlw.sortField.note).innerText,
          contains(e.message("note")));
      expect(
          e.tableHeader("note", mlw.sortField.note).innerText, contains('▼'));

      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.desc);
      e.tableHeader("note", mlw.sortField.note).click();
      expect(m.field, mlw.sortField.note);
      expect(m.direction, mlw.sortDirection.desc);
      e.tableHeader("note", mlw.sortField.note).click();
      expect(m.direction, mlw.sortDirection.asc);
      expect(m.field, mlw.sortField.note);
    });
    test('sortByField', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'strength'),
          ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.date, mlw.sortDirection.asc);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'strength'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
      m.set(mlw.sortField.date, mlw.sortDirection.desc);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'medicationList'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'note'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
    });
    test('sortedMonthKeys', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.sortedMonthKeys(), ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.date, mlw.sortDirection.asc);
      expect(e.sortedMonthKeys(), ['2021-01-01', '2021-01-05', '2021-01-09']);

      m.set(mlw.sortField.strength, mlw.sortDirection.desc);
      expect(e.sortedMonthKeys(), ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.strength, mlw.sortDirection.asc);
      expect(e.sortedMonthKeys(), ['2021-01-01', '2021-01-05', '2021-01-09']);
    });
    test('changeSort', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(m.field, mlw.sortField.date);
      e.changeSort(mlw.sortField.date);
      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.asc);
      e.changeSort(mlw.sortField.strength);
      expect(m.direction, mlw.sortDirection.desc);
      expect(m.field, mlw.sortField.strength);
      e.changeSort(mlw.sortField.strength);
      expect(m.direction, mlw.sortDirection.asc);
      expect(m.field, mlw.sortField.strength);
    });
    test('build', () {
      var m = mlw.MonthTableManager();
      var e = mlw.MonthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.build(), TypeMatcher<Element>());
    });
  });
  test('tableManager', () {
    var called = false;
    var m = mlw.MonthTableManager();
    expect(m.field, mlw.sortField.date);
    expect(m.direction, mlw.sortDirection.desc);
    m.listen(() => called = true);
    m.set(mlw.sortField.strength, mlw.sortDirection.desc);
    expect(m.field, mlw.sortField.strength);
    expect(m.direction, mlw.sortDirection.desc);
    expect(called, isTrue);
    m.set(mlw.sortField.date, mlw.sortDirection.asc);
    expect(m.field, mlw.sortField.date);
    expect(m.direction, mlw.sortDirection.asc);
  });
  test('main', () {
    querySelector('body')
      ?..append(Element.div()
        ..innerText = jsonEncode(testData)
        ..attributes["id"] = "migraineLogData")
      ..append(Element.div()..attributes["id"] = "loading");
    mlw.main();
    expect(querySelector('#loading'), isNull);
  });
}
