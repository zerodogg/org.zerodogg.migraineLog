// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2022   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:MigraineLog/genericwidgets.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:platform/platform.dart';
import 'utils.dart';

void main() {
  var skipGoldenTests = !LocalPlatform().isLinux;

  setTestFile("genericwidgets");
  group('MDTextBox', () {
    testWidgets('Displays text', (WidgetTester tester) async {
      await tester.pumpWidget(materialWidget(MDTextBox(text: "Hello world")));

      expect(find.text('Hello world'), findsOneWidget);
    });
    testWidgets('golden', (WidgetTester tester) async {
      await goldenTest(
        widgetType: MDTextBox,
        widgetInstance: MDTextBox(text: "Hello world"),
        tester: tester,
      );
    }, skip: skipGoldenTests);
  });
  testWidgets('MigraineLogHeader golden', (WidgetTester tester) async {
    await goldenTest(
      widgetType: MigraineLogHeader,
      widgetInstance: MigraineLogHeader(text: "Hello world"),
      tester: tester,
    );
  }, skip: skipGoldenTests);
  group('MigraineLogMonthSwitcherWidget', () {
    testWidgets('golden', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var s = MigraineLogGlobalState();
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      await tester.pumpWidget(
        materialWidgetBuilder((_) => MigraineLogMonthSwitcherWidget(),
            provide: [
              ChangeNotifierProvider.value(value: s),
            ]),
      );

      await goldenTest(
        widgetType: MigraineLogMonthSwitcherWidget,
        name: 'MigraineLogMonthSwitcherWidget',
        tester: tester,
      );
    }, skip: skipGoldenTests);
    testWidgets('interaction', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var s = MigraineLogGlobalState();
      var now = DateTime.now();
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      await tester.pumpWidget(
        materialWidgetBuilder((_) => MigraineLogMonthSwitcherWidget(),
            provide: [
              ChangeNotifierProvider.value(value: s),
            ]),
      );
      expect(find.text("Jan 2021"), findsOneWidget);
      expect(find.byIcon(Icons.arrow_left), findsOneWidget);
      expect(find.byIcon(Icons.arrow_right), findsOneWidget);
      await tester.tap(find.byIcon(Icons.arrow_right));
      await tester.pump();
      expect(find.text("Feb 2021"), findsOneWidget);
      expect(s.currentCalendarMonth.month, 2);
      await tester.tap(find.byIcon(Icons.arrow_left));
      await tester.pump();
      expect(s.currentCalendarMonth.month, 1);
      s.currentCalendarMonth = now;
      await tester.pump();
      expect(s.currentCalendarMonth.month, now.month);
      expect(s.currentCalendarMonth.year, now.year);
      await tester.tap(find.byIcon(Icons.arrow_right));
      await tester.pump();
      expect(s.currentCalendarMonth.month, now.month);
    });
  });
}
