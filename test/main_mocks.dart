// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2024    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:MigraineLog/main.dart';
import 'package:mocktail/mocktail.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:package_info_plus/package_info_plus.dart';

class FakedMigraineLog extends MigraineLog {
  @override
  FakeMigraineLogState createState() => FakeMigraineLogState();
}

class FakeMigraineLogState extends MigraineLogState {
  @override
  // Initialize our MigraineList instance
  void initState() {
    super.initState();
    list = MockList();
    config = MockConfig();
    when(() => list!.loadData()).thenAnswer((_) async {
      return FileLoadStatus.success;
    });
    var statsList = MigraineLogStatisticsList(ofDays: 30);
    statsList.entries = [
      MigraineLogStatisticsEntry(title: "Empty", days: 20, ofDays: 30),
      MigraineLogStatisticsEntry(
          title: "Strong migraine",
          days: 5,
          ofDays: 30,
          strength: MigraineStrength.strongMigraine),
      MigraineLogStatisticsEntry(
          title: "Migraine",
          days: 3,
          ofDays: 30,
          strength: MigraineStrength.migraine),
      MigraineLogStatisticsEntry(
          title: "Headache",
          days: 2,
          ofDays: 30,
          strength: MigraineStrength.headache),
    ];
    when(() => list!.headacheStats(any(), any())).thenReturn(statsList);
    when(() => list!.headacheDaysStats(any())).thenReturn(statsList);
    when(() => list!.entries).thenReturn(30);
    when(() => list!.isNotEmpty).thenReturn(true);
    when(() => list!.newEntry()).thenReturn(MigraineEntry(parentList: list));
    when(() => list!.exists(
          any(),
        )).thenReturn(false);
    when(() => config!.loadConfig()).thenAnswer((_) async {
      return;
    });
    when(() => config!.messages).thenReturn(MigraineLogConfigMessages());
    when(() => config!.medications).thenReturn(MigraineConfigMedicationList());
    when(() => config!.onboardingVersion).thenReturn(9999);
    when(() => config!.neutralLanguage).thenReturn(false);
    when(() => config!.enableAuraStrength).thenReturn(false);
    when(() => config!.enableNoHeadacheStrength).thenReturn(false);
    when(() => config!.displayNoteMarker).thenReturn(false);
  }
}

void packageInfoMock() {
  PackageInfo.setMockInitialValues(
      appName: "Migraine Log",
      packageName: "org.zerodogg.migraineLog",
      version: "0.0.0",
      buildNumber: "0",
      buildSignature: "buildSignature");
}

class MockConfig extends Mock implements MigraineLogConfig {}

class MockList extends Mock implements MigraineList {}
