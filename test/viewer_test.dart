// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2022    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/viewer.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:provider/provider.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mocktail/mocktail.dart';
import 'package:platform/platform.dart';
import 'package:MigraineLog/localehack.dart';
import 'utils.dart';
import 'fakes.dart';

void main() {
  var skipGoldenTests = !LocalPlatform().isLinux;

  setUpAll(() {
    registerFallbackValue(FakeRoute());
  });

  setTestFile('viewer');

  // These two lists are used to iterate over all possible combinations of
  // MigraineStrength and medication state for rendering tests
  var strengths = [
    MigraineStrength.strongMigraine,
    MigraineStrength.migraine,
    MigraineStrength.headache,
    MigraineStrength.auraOnly,
  ];
  var meds = [true, false];

  group('MigraineLogCalendarWidget', () {
    testWidgets('golden', (WidgetTester tester) async {
      var s = MigraineLogGlobalState();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogCalendarWidget(
          onDayPressed: (_, __) {},
          onDayLongPressed: (_) {},
          currentDate: DateTime(2021, 1, 1),
          height: MigraineLogCalendarWidget.maxCalendarHeight,
        ),
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogCalendarWidget,
        tester: tester,
        name: "MigraineLogCalendarWidget",
      );
    }, skip: skipGoldenTests);
    // Here we're building one golden for each kind of registration, combined
    // with each type of medication state (taken/not taken). Together these
    // will build goldens that represent the various states of the calendar.
    for (var strength in strengths) {
      for (var med in meds) {
        for (var note in [true, false]) {
          testWidgets(
              'customDayBuilder strength=' +
                  strength.humanNumeric.toString() +
                  ' w/med=' +
                  med.toString() +
                  (note == true ? (' w/noteIndicator=' + note.toString()) : ''),
              (WidgetTester tester) async {
            var s = MigraineLogGlobalState();
            s.currentCalendarMonth = DateTime(2021, 1, 1);
            await initializeDateFormatting("en", null);
            var c = MigraineLogConfig();
            if (strength == MigraineStrength.auraOnly) {
              c.enableAuraStrength = true;
            }
            c.displayNoteMarker = note;
            MigraineList l = MockMigraineList();
            // Build a fake MigraineEntry whenever something wants to .get() a
            // date. So every date will have a MigraineEntry, and the
            // MigraineEntry will be based upon the value of strength and med.
            when(() => l.get(any())).thenAnswer((_) {
              var e = MigraineEntry();
              e.strength = strength;
              if (med) {
                e.medications.add('any');
              }
              if (note) {
                e.note = "Note";
              }
              return e;
            });
            await tester.pumpWidget(materialWidgetBuilder(
              (context) => MigraineLogCalendarWidget(
                onDayPressed: (_, __) {},
                onDayLongPressed: (_) {},
                currentDate: DateTime(2021, 1, 1),
                height: MigraineLogCalendarWidget.maxCalendarHeight,
              ),
              provide: [
                ChangeNotifierProvider.value(value: c),
                ChangeNotifierProvider.value(value: l),
                ChangeNotifierProvider.value(value: s),
              ],
            ));
            await goldenTest(
              widgetType: MigraineLogCalendarWidget,
              tester: tester,
              name: "MigraineLogCalendarWidget_" +
                  strength.humanNumeric.toString() +
                  '-' +
                  med.toString() +
                  (note ? '-wNoteI' : ''),
            );
            await tester.flush();
          }, skip: skipGoldenTests);
        }
      }
    }
    testWidgets('Pressing callbacks', (WidgetTester tester) async {
      var s = MigraineLogGlobalState();
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      MigraineList l = MockMigraineList();
      // Build a fake MigraineEntry whenever something wants to .get() a
      // date. So every date will have a MigraineEntry, and the
      // MigraineEntry will be based upon the value of strength and med.
      when(() => l.get(any())).thenAnswer((_) {
        var e = MigraineEntry();
        e.strength = MigraineStrength.migraine;
        e.date = DateTime(2021, 1, 1);
        return e;
      });
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      var onDayPressed = false;
      var onDayLongPressed = false;
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogCalendarWidget(
          onDayPressed: (_, __) {
            onDayPressed = true;
          },
          onDayLongPressed: (_) {
            onDayLongPressed = true;
          },
          currentDate: DateTime(2021, 1, 1),
          height: MigraineLogCalendarWidget.maxCalendarHeight,
        ),
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await tester.pumpAndSettle();
      expect(onDayLongPressed, isFalse);
      expect(onDayPressed, isFalse);
      await tester.longPress(find.text("10"));
      expect(onDayLongPressed, isTrue);
      expect(onDayPressed, isFalse);
      onDayLongPressed = false;
      await tester.tap(find.text("10"));
      expect(onDayPressed, isTrue);
      expect(onDayLongPressed, isFalse);
    });
  });
  group('MigraineLogDayViewer', () {
    testWidgets('golden with all content', (WidgetTester tester) async {
      LocaleHack.reset();
      LocaleHack.overrideLocale = 'en';
      await initializeDateFormatting('en');

      var c = MigraineLogConfig();
      MigraineList l = MockMigraineList();
      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.migraine;
      e.medications.addList([
        MigraineMedicationEntry(medication: 'test1', hour: 0, minute: 0),
        MigraineMedicationEntry(medication: 'test2', hour: 0, minute: 0)
      ]);
      e.note = "TESTNOTE";
      when(() => l.exists(any())).thenReturn(true);
      when(() => l.get(any())).thenReturn(e);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogDayViewer(
          currentDate: DateTime(2021, 1, 1),
        ),
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogDayViewer,
        tester: tester,
        name: "MigraineLogDayViewer",
      );
    }, skip: skipGoldenTests);
    // Here we're testing for each kind of registration, combined with each
    // type of medication state (taken/not taken). Together these will build
    // goldens that represent the various states of the calendar.
    for (var strength in strengths) {
      for (var med in meds) {
        for (var note in [true, false]) {
          testWidgets(
              'getTextElements [text] strength=' +
                  strength.humanNumeric.toString() +
                  ' w/med=' +
                  med.toString() +
                  ' &note= ' +
                  note.toString(), (WidgetTester tester) async {
            await initializeDateFormatting("en", null);

            var c = MigraineLogConfig();
            if (strength == MigraineStrength.auraOnly) {
              c.enableAuraStrength = true;
            }
            MigraineList l = MockMigraineList();
            var e = MigraineEntry(parentList: l);
            e.strength = strength;
            MigraineMedicationEntry? testMed;
            if (med) {
              testMed = MigraineMedicationEntry(
                  medication: 'testMedication', hour: 10, minute: 25);
              e.medications.add(testMed);
            }
            if (note) {
              e.note = "TESTNOTE";
            }
            when(() => l.exists(any())).thenReturn(true);
            when(() => l.get(any())).thenReturn(e);
            var i = MigraineLogDayViewer(
              currentDate: DateTime(2021, 1, 1),
            );
            await tester.pumpWidget(materialWidgetBuilder(
              (context) => RichText(text: i.getTextElements(l, c)),
              provide: [
                ChangeNotifierProvider.value(value: c),
                ChangeNotifierProvider.value(value: l),
              ],
            ));
            String message;
            switch (strength) {
              case MigraineStrength.strongMigraine:
                {
                  message = c.messages.strongMigraineMessage();
                }
                break;

              case MigraineStrength.migraine:
                {
                  message = c.messages.migraineMesssage();
                }
                break;
              case MigraineStrength.auraOnly:
                {
                  message = c.messages.auraOnlyMessage();
                }
                break;

              default:
                {
                  message = c.messages.headacheMessage();
                }
            }
            expect(
              find.byWidgetPredicate((w) =>
                  w is RichText && w.text.toPlainText().contains(message)),
              findsOneWidget,
            );
            if (note) {
              expect(
                find.byWidgetPredicate((w) =>
                    w is RichText &&
                    w.text
                        .toPlainText()
                        .contains(i.notePrefixStrig() + ' TESTNOTE')),
                findsOneWidget,
              );
            }
            await tester.flush();
          });
          testWidgets(
              'getTextElements [rendered] strength=' +
                  strength.humanNumeric.toString() +
                  ' w/med=' +
                  med.toString() +
                  ' &note= ' +
                  note.toString(), (WidgetTester tester) async {
            await initializeDateFormatting("en", null);

            var c = MigraineLogConfig();
            if (strength == MigraineStrength.auraOnly) {
              c.enableAuraStrength = true;
            }
            MigraineList l = MockMigraineList();
            var e = MigraineEntry(parentList: l);
            e.strength = strength;
            MigraineMedicationEntry? testMed;
            if (med) {
              testMed = MigraineMedicationEntry(
                  medication: 'testMedication', hour: 10, minute: 25);
              e.medications.add(testMed);
            }
            if (note) {
              e.note = "TESTNOTE";
            }
            when(() => l.exists(any())).thenReturn(true);
            when(() => l.get(any())).thenReturn(e);
            await tester.pumpWidget(materialWidgetBuilder(
              (context) => MigraineLogDayViewer(
                currentDate: DateTime(2021, 1, 1),
              ),
              provide: [
                ChangeNotifierProvider.value(value: c),
                ChangeNotifierProvider.value(value: l),
              ],
            ));
            if (testMed != null) {
              expect(e.medications.takenMeds, isTrue);
              expect(
                find.byWidgetPredicate((w) => w is Table),
                findsOneWidget,
              );
              expect(
                find.text(testMed.medication),
                findsOneWidget,
              );
              // TODO: Test time too
            } else {
              expect(find.text('testMedication'), findsNothing);
            }
            await tester.flush();
          });
        }
      }
    }
  });
  group('MigraineLogViewer', () {
    testWidgets('golden with all content', (WidgetTester tester) async {
      LocaleHack.reset();
      LocaleHack.overrideLocale = 'en';
      await initializeDateFormatting("en", null);

      var c = MigraineLogConfig();
      var s = MigraineLogGlobalState();
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      s.currentDate = DateTime(2021, 1, 1);
      MigraineList l = MockMigraineList();
      var e = MigraineEntry(parentList: l);
      e.strength = MigraineStrength.migraine;
      e.medications.addList([
        MigraineMedicationEntry(medication: 'test1', hour: 0, minute: 0),
        MigraineMedicationEntry(medication: 'test2', hour: 0, minute: 0)
      ]);
      e.note = "TESTNOTE";
      when(() => l.exists(any())).thenReturn(true);
      when(() => l.get(any())).thenReturn(e);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogViewer(),
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogViewer,
        tester: tester,
        name: "MigraineLogViewer",
      );
    }, skip: skipGoldenTests);
    testWidgets('Long press callback', (WidgetTester tester) async {
      var s = MigraineLogGlobalState();
      s.currentDate = DateTime(2021, 1, 1);
      s.currentCalendarMonth = s.currentDate;
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      MigraineList l = MockMigraineList();
      // Build a fake MigraineEntry whenever something wants to .get() a
      // date. So every date will have a MigraineEntry, and the
      // MigraineEntry will be based upon the value of strength and med.
      when(() => l.get(any())).thenAnswer((_) {
        var e = MigraineEntry();
        e.strength = MigraineStrength.migraine;
        e.date = DateTime(2021, 1, 1);
        return e;
      });
      when(() => l.exists(any())).thenReturn(true);
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      var navO = MockNavigatorObserver();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogViewer(),
        navObserver: navO,
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
        ],
        routes: {
          '/editor': (_) => Text("editor"),
        },
      ));
      await tester.pumpAndSettle();
      verify(() => navO.didPush(any(), any()));
      await tester.longPress(find.text("10"));
      var captured = verify(() => navO.didPush(captureAny(), any())).captured;
      Route newRoute = captured[0] as Route;
      expect(newRoute.settings.name, '/editor');
    });
    testWidgets('Tap callback', (WidgetTester tester) async {
      var s = MigraineLogGlobalState();
      s.currentDate = DateTime(2021, 1, 1);
      s.currentCalendarMonth = s.currentDate;
      await initializeDateFormatting("en", null);
      var c = MigraineLogConfig();
      MigraineList l = MockMigraineList();
      // Build a fake MigraineEntry whenever something wants to .get() a
      // date. So every date will have a MigraineEntry, and the
      // MigraineEntry will be based upon the value of strength and med.
      when(() => l.get(any())).thenAnswer((_) {
        var e = MigraineEntry();
        e.strength = MigraineStrength.migraine;
        e.date = DateTime(2021, 1, 1);
        return e;
      });
      when(() => l.exists(any())).thenReturn(true);
      s.currentCalendarMonth = DateTime(2021, 1, 1);
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogViewer(),
        provide: [
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await tester.pumpAndSettle();
      expect(s.currentDate.year, 2021);
      expect(s.currentDate.month, 1);
      expect(s.currentDate.day, 1);
      await tester.tap(find.text("10"));
      await tester.pumpAndSettle();
      expect(s.currentDate.year, 2021);
      expect(s.currentDate.month, 1);
      expect(s.currentDate.day, 10);
    });
    // TODO
    testWidgets('MediaQuery', (WidgetTester tester) async {}, skip: true);
  });
}

class MockMigraineList extends Mock implements MigraineList {}
