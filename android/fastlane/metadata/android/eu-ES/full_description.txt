Migraine Log-ek migraina-krisiak (edo buruko beste min batzuk) erraz erregistratzeko aukera ematen dizu. Ahalik eta errazena izateko egina dago eta gertaeren data eta intentsitatea baino ez ditu behar. Horrez gain, hartutako botiken eta krisiei buruzko oharren xehetasunak gehitzeko aukera ematen du. Era horretan, migrainak monitorizatu ditzakezu eta zuri edota zure medikuari tratamendu egokia aurkitzen lagundu.


Interfazea diseinatzerako orduan migrainak pairatzen dituztenak izan dira kontuan eta aplikazioak kolore leun eta ilunak erabiltzen ditu krisi betean erabili ahal izateko.


Migraine Log-ek pribatutasuna errespetatzen du eta ez du sarerako sarbiderik eskatzen; beraz, aplikazioaren esportazio-funtzioa esplizituki erabiltzen ez baduzu, zure informazioa ez da zure gailutik irtengo.