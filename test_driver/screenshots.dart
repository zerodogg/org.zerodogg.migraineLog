// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// NOTE: This file is not intended to be run directly. It contains several
// placeholders: OVERRIDE_LOCALE, MIGRAINE_MEDICATION and PAINKILLERS.
// These are intended to be replaced and then the screenshots flutter_driver
// instance run to get screenshots for a single locale. The
// scripts/prepareForScreenshots handles replacing the placeholders, while the
// 'screenshots' make target ties this file, screenshots_test.dart and
// prepareForScreenshots together, and runs the set once for each locale
// supported for the app in order to build the screenshots.

import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/main.dart' as app;
import 'package:flutter/services.dart' show SystemChrome, SystemUiOverlayStyle;
import 'package:MigraineLog/localehack.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'dart:io';

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension();

  if (!Platform.isIOS) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  }

  WidgetsApp.debugAllowBannerOverride = false;
  LocaleHack.overrideLocale = 'OVERRIDE_LOCALE';

  runApp(app.MigraineLog(customInitialization: (state, config, list) {
    config.onboardingVersion = 1;
    config.medications.add("MIGRAINE_MEDICATION");
    config.medications.add("PAINKILLERS");
    state.currentCalendarMonth = DateTime(2021, 1, 1);
    state.currentDate = DateTime(2021, 1, 1);
    list.loadDataFromJSON(
      '{"dataVersion":1,"entries":{"2021-01-12":{"strength":2,"note":"","medicationList":[]},"2020-12-27":{"medicationList":[],"note":"","strength":2},"2021-01-10":{"strength":2,"medicationList":[],"note":""},"2021-02-06":{"medicationList":["MIGRAINE_MEDICATION"],"note":"","strength":2},"2021-02-05":{"note":"","medicationList":[],"strength":1},"2021-01-23":{"note":"","medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"strength":2},"2021-02-03":{"medicationList":["MIGRAINE_MEDICATION"],"note":"","strength":3},"2021-01-25":{"note":"","medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"strength":2},"2021-01-30":{"strength":1,"medicationList":["PAINKILLERS"],"note":""},"2021-01-04":{"strength":1,"medicationList":[],"note":""},"2021-01-01":{"note":"","medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"strength":2},"2021-01-02":{"strength":1,"medicationList":[],"note":""},"2020-12-29":{"note":"","medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"strength":2},"2021-02-07":{"strength":3,"note":"","medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"]},"2021-01-15":{"strength":3,"medicationList":[],"note":""},"2021-01-13":{"medicationList":["MIGRAINE_MEDICATION"],"note":"","strength":2},"2021-01-27":{"note":"","medicationList":["PAINKILLERS"],"strength":2},"2020-12-26":{"medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"note":"","strength":3},"2021-02-04":{"note":"","medicationList":["MIGRAINE_MEDICATION"],"strength":2},"2021-01-21":{"strength":2,"note":"","medicationList":[]},"2021-02-02":{"note":"","medicationList":["Migea"],"strength":3},"2021-01-20":{"medicationList":["MIGRAINE_MEDICATION"],"note":"","strength":3},"2021-01-16":{"note":"","medicationList":[],"strength":2},"2021-01-08":{"strength":3,"medicationList":[],"note":""},"2020-12-30":{"note":"","medicationList":["PAINKILLERS"],"strength":2},"2021-01-18":{"strength":2,"note":"","medicationList":["MIGRAINE_MEDICATION"]},"2021-01-06":{"note":"","medicationList":["MIGRAINE_MEDICATION"],"strength":3},"2021-01-05":{"medicationList":["MIGRAINE_MEDICATION","PAINKILLERS"],"note":"","strength":3},"2020-12-31":{"medicationList":["MIGRAINE_MEDICATION"],"note":"","strength":2}}}',
    );
    // Populate the last 30 days
    var thirtyDaysAgo = DateTime.now().subtract(Duration(days: 30));
    var current = DateTime.now();
    var now = current;

    var medication = 9;
    var strong = 5;
    var migraine = 10;
    var headache = 3;

    while (thirtyDaysAgo.isBefore(current)) {
      var entry = MigraineEntry();
      entry.date = current;
      // Leave the first day with no entry so that triggering the add/edit
      // doesn't result in a warning about an already existing event
      if (current.isAtSameMomentAs(now)) {
        current = current.subtract(Duration(days: 1));
        continue;
      }
      current = current.subtract(Duration(days: 1));
      if (--strong > 0) {
        entry.strength = MigraineStrength.strongMigraine;
      } else if (--migraine > 0) {
        entry.strength = MigraineStrength.migraine;
      } else if (--headache > 0) {
        entry.strength = MigraineStrength.headache;
      } else {
        continue;
      }
      if (--medication > 0) {
        entry.medications.add("Test");
      }
      list.set(entry);
    }
  }));
}
