# Migraine Log

A simple multi-platform headache diary, written in [dart](https://dart.io)
using the [flutter](https://flutter.io) framework.

## Getting the release version of the app

Migraine Log is available for Android on [Google
Play](https://play.google.com/store/apps/details?id=org.zerodogg.migraineLog)
and [F-Droid](https://f-droid.org/en/packages/org.zerodogg.migraineLog/), as
well as iOS on [the app
store](https://apps.apple.com/no/app/migraine-log/id1636946550?platform=iphone).
Packages are also published to the GitLab [package
registry](https://gitlab.com/mglog/org.zerodogg.migraineLog/-/packages)
(though whenever possible using one of the aforementioned appstores should be
preferred, since that makes sure you receive updates).

## Building

Use the included Makefile to build Migraine Log. By default it will build as
`org.zerodogg.migraineLogTest` which will not conflict with release versions,
which are `org.zerodogg.migraineLog`. You can switch between the two using the
`setRelVariant` (…migraineLog) and `setDevVariant` (…migraineLogTest) targets.
For tags (releases) the CI does the equivalent of `make setRelVariant` before
building the packages. Snapshots are built with the equivalent of `make
setDevVariant` (and thus snapshots published on GitLab (after 2021-03-12) does
not conflict with the release version of the app, and can exist alongside it).

### Targets

`androiddistrib`: builds per-architecture APKs, one fat APK and one appbundle

`androidrelease`: same as distrib, but runs `setRelVariant` before building and
`setDevVariant` when done.

`buildlinux`: builds a Linux binary

`linuxrelease`: builds a release Linux binary

`iosdistrib`: builds an iOS binary

`prepare`: generates the source files needed for `Intl` and compiles our
javascript assets. This is required for Migraine Log to build, since these
autogenerated files aren't checked into git. You must run this before
**`flutter run`** or similar to work.

## Credits

iOS development environment generously provided by
[codemagic.io](https://codemagic.io).

Linux and Android CI environment generously provided by [GitLab Solutions for
Open Source Projects](https://about.gitlab.com/solutions/open-source/).

Translations by @jofrev (DE), Diego (ES), Mika Latvala (FI),
@poipoipoipoipoipoipoipoipoi (Chinese), @Valanna (FR) and @xabirequejo (EU),
@JustMarkov (RU), @CallMeFlanby (PT).

## Donations

You may [buy me a coffee](https://www.buymeacoffee.com/zerodogg) if you wish.
This is completely optional, and I appreciate you using my software regardless.

## License

Migraine Log is Copyright © Eskild Hustvedt 2021-2025

Migraine Log is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Migraine Log is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/gpl-3.0.txt.
