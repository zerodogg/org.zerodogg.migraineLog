# Contributing a translation

## New translation

If you want to contribute a new translation, start by copying the file
`i18n/intl_messages.arb` to `i18n/YOURLANGUAGE.arb` where YOURLANGUAGE is the
two-letter language code for the language you wish to contribute.

Then edit the new `YOURLANGUAGE.arb` file with a text editor. Do not change
values on the left-hand side, but on the right-hand side. For instance, given
the line `"General": "General",`, where the translation for Norwegian would be
`"Generell"` change this line to `"General":"Generell",`. Do not change any of
the lines where the string starts with `@`, as those are system strings, for
instance `"@Use neutral language": {`.

The `arb`-files use the [JSON](https://www.json.org/) syntax, so the files must
be valid JSON to work.

If you want to build your own test versions of the app, you will also need to
edit `lib/localehack.dart` and add your two-character language code to the
`supportedLocales` and `flutterLocaleList` arrays. This is not required if you
don't want to build your own test versions, we will do it for you if you
haven't done it.

## Updating an existing translation

Edit the `YOURLANGUAGE.arb` file, changing any new strings that needs to be
added. If you want to quickly find out which strings need changing and are
comfortable with the command line, run `make stats` in the i18n directory.

## Submitting a translation

There are two ways to do this. One is by creating (or updating) an issue in the
gitlab issue tracker: https://gitlab.com/mglog/org.zerodogg.migraineLog/-/issues

The other is to fork the GitLab repository and submitting a merge request.
Either works, it's mostly a matter of personal preference.

## What about new versions?

If you've submitted a translation or a translation update, then you will get
added to the GitLab repository as a `guest`. This means that issues can be
assigned to you. Before major releases a new issue will be created and assigned
to you wrt. updating translations for a new release. You may have only a short
period of time to respond to the issue before release, however if the release
goes ahead without your updated translation then a new patch release will be
made once you have submitted your translation.
