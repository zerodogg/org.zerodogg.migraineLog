{
  "@@last_modified": "2025-01-19T19:53:11.738830",
  "@@locale": "pt",
  "General": "Geral",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Continue": "Seguinte",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use neutral language": "Linguagem neutra",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Usar o termo \"dores de cabeça\" invés de \"enxaqueca\"",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable \"aura only\" strength": "Ativar intensidade \"só aura\"",
  "@Enable \"aura only\" strength": {
    "description": "Toggle button that enables (or disables) use of \"aura only\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Let me set \"aura only\" as a the strength of an attack": "Permitir \"só aura\" como intensidade das dores",
  "@Let me set \"aura only\" as a the strength of an attack": {
    "description": "This is used as a description for the 'aura only' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Enable notes on days without attacks": "Ativar notas em dias sem dores",
  "@Enable notes on days without attacks": {
    "description": "Toggle button that enables (or disables) use of \"no headache\" as a \"strength\" option",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Lets you choose a \"no headache\" as the strength of an attack": "Permitir \"sem enxaquecas\" como intensidade das dores",
  "@Lets you choose a \"no headache\" as the strength of an attack": {
    "description": "This is used as a description for the 'no headache' config option. Feel free to rephrase.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show note indicators": "Mostrar notas",
  "@Show note indicators": {
    "description": "Toggle button that enables/disables showing a marker on dates with notes in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show a small dot on dates where you have written a note": "Marcar os dias com notas com um ponto indicador",
  "@Show a small dot on dates where you have written a note": {
    "description": "The 'dot' is a small grey square, displayed in the upper right hand corner of the date number in the calendar",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "What kind of headache do you have?": "Que enxaqueca estás a ter?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Select this if you're unsure": "Seleciona esta opção se não tens a certeza",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine": "Enxaqueca",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Outras dores (não específicas,  cefaleias, etc.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Settings": "Definições",
  "@Settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Configuration": "Configuração",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication list": "Lista dos medicamentos",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "Mudar aqui a lista dos medicamentos não afetará ocurrências já registadas.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "Clica em \"Continuar\" se não quiseres adicionar a tua medicação. Serão adicionados medicamentos habituais.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add a new medication": "Adicionar novo medicamento",
  "@Add a new medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Undo": "Anular",
  "@Undo": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_deletedElement": "Apagar \"{medication}\"",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders_order": [
      "medication"
    ],
    "placeholders": {
      "medication": {}
    }
  },
  "No registration": "Sem registos",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Headache": "Enxaqueca leve",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Mild headache": "Dores de cabeça leves",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Moderate headache": "Dores de cabeça normais",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong migraine": "Enxaqueca forte",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strong headache": "Dores de cabeça fortes headache",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura only": "Só aura",
  "@Aura only": {
    "description": "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "No headache": "Sem enxaqueca",
  "@No headache": {
    "description": "Used as the 'strength' of a day where the user had no headache (for registering notes on days without symptoms)",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine medication": "Medicação para as enxaquecas",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Seizure medication": "Medicação para os ataques",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Painkillers": "Analgésicos",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Edit": "Editar",
  "@Edit": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register": "Registar",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "You must select a strength": "Escolher uma intensidade",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Save": "Guardar",
  "@Save": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "editingEntryNewDate": "Editar registo de {editDate}. Se se mudar a data, este registo será transferido para uma data nova.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders_order": [
      "editDate"
    ],
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Atenção: neste dia já existe um registo. Se se continuar, os dados serão substituídos.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Date": "Data",
  "@Date": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "change date": "mudar data",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Strength": "Intensidade",
  "@Strength": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "A dor é subjetiva. A única pessoa que pode saber qual é a melhor opção és tu. Em caso de indecisão, escolhe a mais intensa entre opções que tens.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform most activities": "Incapacidade de fazer a maioria das tarefas",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Unable to perform some activities": "Incapacidade de fazer algumas tarefas",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Able to perform most activities": "É possivel fazer quase todas as tarefas",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Aura symptoms only": "Só sintomas com aura",
  "@Aura symptoms only": {
    "description": "Migraine strength help text for aura only",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Close": "Fechar",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Which strength should I choose?": "Que intensidade devo escolher?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications taken": "Medicação tomada",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Note": "Nota",
  "@Note": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "(none)": "(nada)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medications": "Medicamentos",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log": "Diário de enxaquecas",
  "@Migraine Log": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hide": "esconder",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Hide this month": "Esconder este mês",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "hidden:": "escondido:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Total headache days": "Dias com enxaquecas (total)",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken medication": "Medicação tomada",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Filter:": "Filtro:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "show": "mostrar",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "everything": "tudo",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "months": "meses",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "List days with no entries": "Lista dos dias sem registos",
  "@List days with no entries": {
    "description": "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_generatedMessage": "Gerado por Diário de enxaquecas versão {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders_order": [
      "version"
    ],
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "A carregar...",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Previous month": "Mês anterior",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Next month": "Mês seguinte",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "_currentMonth": "Mês atual: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders_order": [
      "month"
    ],
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "O Diário das Enxaquecas ajuda-te a manter um registo diário de enxaquecas. Assim que adicionares um registo, esta vista será substituída por estatísticas.\n\nPara adicionar um novo registo carregar no botão \"+\".\n\nPara mais ajuda, clica em \"Help\" no meno no canto superior direito.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Help": "Ajuda",
  "@Help": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Icons in the calendar": "Ícones no calendário",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Colours": "Cores",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "Um traço debaixo de um dia no calendário significa que há um registo nesse dia, sem que fosse tomada qualquer medicação.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "Um círculo à volta de uma dia no calendário quer dizer que há um registo nesse dia e foi tomada medicação.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": "Um ponto no canto superior direito de uma data significa que o registo tem uma nota.",
  "@A dot in the upper right hand corner of a date indicates that the entry on that date includes a note.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "O Diário das Enxaquecas usar cores para distinguir as intensidades das dores. O traço/círculo no calendário e a cor do registo usarão a cores dependendo da intensidade.",
  "@Throughout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "dayString": "{number,plural, =1{{number} dia}=2{{number} dias}other{{number} dias}}",
  "@dayString": {
    "description": "How many days there are. This isn't as complicated as it looks. Unless you know you need to modify the pluralization rules (in which case see https://pub.dev/documentation/intl/latest/intl/Intl/plural.html) you should just change the string 'day' and 'days' here into your local equivalent. For instance, the Norwegian version of this string is: '{number,plural, =1{{number} dag}=2{{number} dagar}other{{number} dagar}}'. number will always be a positive integer (never zero).",
    "type": "text",
    "placeholders_order": [
      "number"
    ],
    "placeholders": {
      "number": {}
    }
  },
  "Successfully imported data": "Importação de dados com sucesso",
  "@Successfully imported data": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "Ficheiro danificado ou não pode ser importado",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "Este ficheiro não parece ser do Diário de Enxaquecas (html)",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "Este ficheiro vem de uma versão mais recente do Diário de Enxaquecas. Atualiza primeiro a aplicação.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "An unknown error occurred during import.": "Erro desconhecido durante a importação.",
  "@An unknown error occurred during import.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import failed:": "Importação falhou:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "A tua versão do Diário de Enxaquecas é demasiado antiga para ler os dados que tens. Por favor atualiza o Diário de Enxaquecas.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": "Falha ao carregar o ficheiro de configuração. Os teus dados estão salvaguardados, podes repor as definições para reparar este problema (terás de voltar a introduzir a tua medicação).",
  "@Your settings file failed to load. Your data is safe, and you may reset your settings to fix this issue (you will need to enter your medications again).": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "If you want to report this issue, take a screenshot of the following information:": "Se quiseres reportar este problema, faz uma captura de ecrã com a seguinte informação:",
  "@If you want to report this issue, take a screenshot of the following information:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Reset settings": "Repor definições",
  "@Reset settings": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Error": "Erro",
  "@Error": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "O Diário de Enxaquecas é um software grátis: podes distribuir e/ou alterar dentro dos termos da licença públic geral GNU publicada pela Free Software Foundation, na versão 3 da mesma, ou (por escolha própria) qualquer versão superior.\n\nEste programa é distribuído na esperança de ser útil, mas SEM GARANTIA; mesmo sem garantia implícita de COMERCIALIZAÇÃO ou FINALIDADE ESPECÍFICA.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Copyright ©": "Direitos de autor ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "See the": "Ver também",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "for details.": "para mais detalhes.",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "About": "Acerca",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Exit": "Sair",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Export": "Exportar",
  "@Export": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Import": "Importar",
  "@Import": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Statistics": "Estatísticas",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Calendar": "Calendário",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Home": "Início",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Translated by": "Traduzida por",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "TRANSLATED_BY": "Flávio Correia Marta",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "deletedDateMessage": "{fmtDate} apagada",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders_order": [
      "fmtDate"
    ],
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Apagar",
  "@Delete": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Add": "Adicionar",
  "@Add": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Show action menu": "Mostrar menu de ações",
  "@Show action menu": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "lastNDaysMessage": "Últimos {days} dias",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders_order": [
      "days"
    ],
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "sem toma de medicação",
  "@took no medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "tookMedsMessage": "toma de {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders_order": [
      "meds"
    ],
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Nota:",
  "@Note:": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Register medications": "Apontar medicação",
  "@Register medications": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Medication": "Medicação",
  "@Medication": {
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "Taken": "Ingestão",
  "@Taken": {
    "description": "This is used in the header of the 'list of medications' table in the viewer. The column it refers to will contain the time that a medication was taken",
    "type": "text",
    "placeholders_order": [],
    "placeholders": {}
  },
  "takenAt": "Toma de {medication} às {time}",
  "@takenAt": {
    "description": "medication is the name of one of the users medications, time is a time in either 24H or 12H format, depending on locale",
    "type": "text",
    "placeholders_order": [
      "medication",
      "time"
    ],
    "placeholders": {
      "medication": {},
      "time": {}
    }
  }
}