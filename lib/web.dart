// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// This component will get embedded into exported HTML in order to parse the
// data and generate HTML.
//
// This is all rather suboptimal since the dart boilerplate that will get
// included will make this file, even minified and optimized, over 60k,
// significantly increasing the size of our exported file. What this DOES let
// us do, however, is test the html-rendering bits along with the rest of the
// app, which probably makes this a worthwhile tradeoff.

import 'dart:html';
import 'dart:convert';
import 'package:meta/meta.dart';
import 'definitions.dart';

String? debugHint;

class MigraineLogWebBase {
  final Map data;
  MonthTableManager? manager;

  MigraineLogWebBase({required this.data, this.manager});

  @visibleForTesting

  /// Converts a "month" string from data into a human-readable month string
  String getHumanMonthString(String month) {
    assert(data['strings']['months'][month] != null);
    return data['strings']['months'][month] ?? '?';
  }

  /// Converts a strength string (stringified int) into a strength message
  String? strength(String str) {
    var message = data['strings']['strength'][str];
    if (int.parse(str) > 0) {
      return message + ' (' + str + ')';
    }
    return message;
  }

  /// Retrieves a message by id, the message is provided by Migraine Log at
  /// export time for use here
  String message(String id) {
    assert(data['strings']['messages'][id] != null);
    return data['strings']['messages'][id] ?? id + '?';
  }

  /// Converts DateTime into a formatted, localized date string
  String formattedDateTime(DateTime dt) {
    Map? dateStrings = data['strings']['messages']['dateStrings'];
    assert(dateStrings != null);

    int wday = dt.weekday - 1; // dart starts the week on day 1, not 0
    int month = dt.month - 1; // dart starts months on 1, not 0
    String wdayLocalized = dateStrings!['weekdays'][wday];
    String monthLocalized = dateStrings['months'][month];
    return wdayLocalized +
        ', ' +
        dt.day.toString() +
        '. ' +
        monthLocalized +
        ' ' +
        dt.year.toString();
  }

  @visibleForTesting
  Map getMonthWithEmpty(String month) {
    var base = data['data'][month];
    Map withEmpty = {};
    var yearI = int.parse(month.substring(0, 4));
    var monthI = int.parse(month.substring(4, 6));
    var processDate = DateTime(yearI, monthI);
    while (processDate.month == monthI) {
      var dateStr = processDate.year.toString() +
          '-' +
          processDate.month.toString().padLeft(2, '0') +
          '-' +
          processDate.day.toString().padLeft(2, '0');
      if (base[dateStr] != null) {
        withEmpty[dateStr] = base[dateStr];
      } else {
        withEmpty[dateStr] = {
          "date": formattedDateTime(DateTime.parse(dateStr)),
          "strength": -100,
        };
      }
      processDate = processDate.add(Duration(days: 1));
      // Don't list dates after today
      if (processDate.isAfter(DateTime.now())) {
        break;
      }
    }
    return withEmpty;
  }

  @visibleForTesting

  /// Retrieves a single month by its string representation
  Map? getMonth(String month) {
    assert(data['data'][month] != null);
    if (manager != null && manager!.displayEmptyDays) {
      return getMonthWithEmpty(month);
    }
    return data['data'][month];
  }

  @visibleForTesting

  /// Retrieves a single month's summary by its string representation
  Map? getMonthSummary(String month) {
    return data['summaries'][month];
  }

  /// Builds a stringified list of medications
  String medicationListToStr(List? list) {
    if (list != null && list.isNotEmpty) {
      String msg = '';
      for (var entry in list) {
        if (entry['timeString'] != '-') {
          msg += entry['timeString'] + ' ';
        }
        msg += entry['name'] + "\n";
      }
      return msg;
    }
    return message('none');
  }
}

class MonthTableManager {
  void listen(Function cb) {
    cbs.add(cb);
  }

  void set(sortField setField, sortDirection setDirection) {
    field = setField;
    direction = setDirection;
    notify();
  }

  void notify() {
    for (var cb in cbs) {
      cb();
    }
  }

  @visibleForTesting
  List<Function> cbs = [];

  /// The current sort field
  @visibleForTesting
  sortField field = sortField.date;

  /// The current sort direction
  @visibleForTesting
  sortDirection direction = sortDirection.desc;

  /// If we're to display empty days in the month or not
  bool displayEmptyDays = false;
}

/// The field to sort a table by
enum sortField { date, strength, medication, note }

/// The sorting direction
enum sortDirection { desc, asc }

class MonthTable extends MigraineLogWebBase {
  MonthTable({
    required this.strMonth,
    required super.manager,
    required super.data,
  }) {
    manager!.listen(() => build());
  }

  /// The string representation of the month we're rendering
  final String strMonth;

  /// Our current container (table-element). This changes throughout the
  /// lifetime of the object.
  @visibleForTesting
  Element? container;

  /// A map of sortFields to their string key equivalents. Used to be able to
  /// map a sort direction to a field contained in the data.
  Map fieldToKey = {
    sortField.note: 'note',
    sortField.medication: 'medicationList',
    sortField.strength: 'strength',
  };

  /// Returns the `field` in `source`. If it is null it returns an empty
  /// string. If it is a list then it gets converted to a String. If it is a
  /// String or int then it gets returned directly.
  dynamic getField(Map source, String field) {
    var value = source[field];
    if (value == null) {
      return '';
    }
    assert(value is String || value is List || value is int);
    if (value is String) {
      return value;
    } else if (value is int) {
      return value;
    } else if (value is List) {
      return value.join('');
    }
  }

  /// Returns the current sorting arrow for the selected field, as an Element.
  /// This element will always contain the arrow for the current direction, but
  /// unless forField is the current `field` then it will have its visibility
  /// set to hidden. This is so that it takes up space in the dom so text
  /// position doesn't jump around by having the arrow added and removed.
  Element sortingArrow(sortField forField) {
    String sorter = ' ' + (manager!.direction == sortDirection.asc ? '▲' : '▼');
    var entry = Element.span()
      ..innerText = sorter
      ..className = "sortArrow";
    if (forField != manager!.field) {
      entry.style.visibility = 'hidden';
    }
    return entry;
  }

  /// Builds a single table header (th-element) and hooks up onClick listeners
  /// for changing the sort direction, looks up the message string for the
  /// header and inserts a sorting arrow
  Element tableHeader(String msgField, sortField sortBy) {
    return Element.th()
      ..innerText = message(msgField)
      ..append(sortingArrow(sortBy))
      ..onClick.listen((_) => changeSort(sortBy));
  }

  /// Sorts the list toSort by the value of `field` in the `source` map
  List sortByField(List toSort, Map source, String field) {
    if (manager!.direction == sortDirection.desc) {
      toSort.sort((a, b) =>
          getField(source[b], field).compareTo(getField(source[a], field)));
    } else {
      toSort.sort((a, b) =>
          getField(source[a], field).compareTo(getField(source[b], field)));
    }
    return toSort;
  }

  /// Retrieves a list of keys in the month strMonth that is sorted by the
  /// currently selected field and direction
  List sortedMonthKeys() {
    Map month = getMonth(strMonth)!;
    List sorted = month.keys.toList();

    /// Date is special, since we use the key itself, so sort that here.
    if (manager!.field == sortField.date) {
      if (manager!.direction == sortDirection.desc) {
        sorted.sort((a, b) => b.compareTo(a));
      } else {
        sorted.sort((a, b) => a.compareTo(b));
      }
    } else if (manager!.field == sortField.strength) {
      sorted = sortByField(sorted, month, fieldToKey[manager!.field]);
    } else {
      assert(fieldToKey[manager!.field] != null);
      sorted = sortByField(sorted, month, fieldToKey[manager!.field]);
    }
    return sorted;
  }

  /// Changes the sorting field and sets the direction to desc, or, if the
  /// current field is the same field we're changing to, changes the sorting
  /// direction. In either case this will trigger a build().
  void changeSort(sortField changeToField) {
    if (changeToField == manager!.field) {
      manager!.set(
          changeToField,
          manager!.direction == sortDirection.asc
              ? sortDirection.desc
              : sortDirection.asc);
    } else {
      manager!.set(changeToField, sortDirection.desc);
    }
  }

  /// Builds this table and injects it into the HTML. This is also used to
  /// rebuild the table when something changes, like the sorting, in which case
  /// it will replace the table already in the DOM with the updated table.
  Element build() {
    Element table = Element.table()..className = "table";
    Map? month = getMonth(strMonth);
    table.append(
      Element.tag('thead')
        ..append(
          Element.tr()
            ..append(tableHeader('date', sortField.date))
            ..append(tableHeader('strength', sortField.strength))
            ..append(tableHeader('takenMeds', sortField.medication))
            ..append(tableHeader('note', sortField.note)),
        ),
    );
    var tbody = Element.tag('tbody');
    table.append(tbody);
    for (var date in sortedMonthKeys()) {
      Map day = month![date];
      String note = "";
      if (day['note'] != null) {
        note = day['note'];
      }
      tbody.append(
        Element.tr()
          ..append(Element.td()..innerText = day['date'])
          ..append(Element.td()
            ..innerText = day['strength'] != -100
                ? strength(day['strength'].toString())!
                : '')
          ..append(Element.td()
            ..innerText = medicationListToStr(day['medicationList']))
          ..append(Element.td()..innerText = note),
      );
    }
    if (container != null) {
      container!.replaceWith(table);
    }
    container = table;
    return table;
  }

  /// Retrieves the root element for this table. Typically a Element.table
  Element? get element {
    if (container == null) {
      build();
    }
    return container;
  }
}

class dataToHTML extends MigraineLogWebBase {
  Element? container;

  List<String> hiddenMonths = [];
  dataToHTML({
    required super.data,
  }) {
    manager ??= MonthTableManager();
  }

  int monthLimit = -1;

  /// Sorts a list in descending order
  List sorted(List thing) {
    thing.sort((a, b) => b.compareTo(a));
    return thing;
  }

  /// Generates DOM elements that represent the summary of a single month
  Element monthSummary(String strMonth) {
    Map month = getMonthSummary(strMonth)!;
    Element table = Element.table();
    for (var type in month['specificSummaries'].keys) {
      table.append(Element.tr()
        ..append(Element.td()..innerText = type)
        ..append(Element.td()..innerText = month['specificSummaries'][type]));
    }
    if (month["medicationDays"] != "0") {
      table.append(Element.tr()
        ..append(Element.td()..innerText = message("takenMeds"))
        ..append(Element.td()..innerText = month['medicationDays']));
    }
    table.append(Element.tr()
      ..append(Element.td()..innerText = message("totalHeadacheDays"))
      ..append(Element.td()..innerText = month['headacheDays']));
    return table;
  }

  /// Retrieves the "header" for a month.
  /// This also handles hiding/showing individual months.
  Element monthHeader(String strMonth) {
    var container = Element.tag('h2')
      ..innerText = getHumanMonthString(strMonth);
    container.append(Element.tag('span')
      ..innerText = '[' + message('hide') + ']'
      ..className = 'hide-month'
      ..title = message('hideTooltip')
      ..onClick.listen((_) {
        var month = container.closest('.month')!;
        var replacement = Element.div()
          ..innerText = '(' +
              message('hidden') +
              ' ' +
              getHumanMonthString(strMonth) +
              ')'
          ..className = 'hidden-month';
        replacement.onClick.listen((_) {
          replacement.replaceWith(month);
        });
        month.replaceWith(replacement);
        container.closest('.month')!.remove();
      }));
    return container;
  }

  /// Generates DOM elements that represent a single month
  Element monthToHTML(String strMonth) {
    Element container = Element.div()..className = 'month';
    container.append(monthHeader(strMonth));
    Element table =
        MonthTable(data: data, strMonth: strMonth, manager: manager!).element!;
    container
      ..append(monthSummary(strMonth))
      ..append(table);
    return container;
  }

  /// Firefox has a bug where table borders disappear in the print preview when
  /// they're 1px in width, so we double it for printing on firefox. This is a
  /// ugly hack, but needed to work around the firefox bug.
  void addFirefoxHack(Element to) {
    if (window.navigator.userAgent.contains('Firefox')) {
      to.append(Element.tag('style')
        ..innerText = '@media print { table, th, td {border: 2px solid black} }'
        ..attributes['type'] = 'text/css');
    }
  }

  void rebuild() {
    var oldContainer = container;
    try {
      container = Element.div();
      constructUI();
      if (oldContainer != null) {
        oldContainer.replaceWith(container!);
      }
    } catch (e) {
      oldContainer!.replaceWith(fatalErrorMessage(e));
    }
  }

  Element buildFilterUI() {
    Map months = data['data'];
    var filterContainer = Element.div()
      ..className = 'filter'
      ..append(Element.tag('b')..innerText = message('filter') + ' ');
    var filterSelect = Element.tag('select')
      ..onChange.listen((ev) {
        var selected = int.parse((ev.currentTarget as SelectElement).value!);
        if (selected != monthLimit) {
          monthLimit = selected;
          rebuild();
        }
      });
    var filterCheckbox = Element.tag('input')
      ..attributes['type'] = 'checkbox'
      ..attributes['id'] = 'emptyDaysCheckbox'
      ..className = 'ml-1'
      ..onChange.listen((ev) {
        manager!.displayEmptyDays = !manager!.displayEmptyDays;
        rebuild();
      });
    var filterText = Element.tag('label')
      ..attributes['for'] = 'emptyDaysCheckbox'
      ..text = message('listEmptyDays');
    if (manager!.displayEmptyDays) {
      filterCheckbox.attributes['checked'] = 'checked';
    }
    filterSelect.append(Element.tag('option')
      ..attributes['id'] = 'monthNoFilter'
      ..attributes['value'] = "-100"
      ..innerText = message('everything'));

    var filterLimit = 3;
    while (filterLimit < months.length) {
      filterSelect.append(Element.tag('option') as OptionElement
        ..selected = monthLimit == filterLimit
        ..attributes['id'] = 'monthNoFilter'
        ..attributes['value'] = filterLimit.toString()
        ..innerText = filterLimit.toString() + ' ' + message('months'));

      if (filterLimit >= 6) {
        filterLimit += 3;
      } else {
        filterLimit += 1;
      }
    }
    if (months.length > 3) {
      filterContainer
        ..append(Element.tag('label')
          ..attributes['for'] = 'monthNoFilter'
          ..innerText = message('show') + ' ')
        ..append(filterSelect);
    }
    return filterContainer
      ..append(filterCheckbox)
      ..append(filterText);
  }

  void constructUI() {
    Map months = data['data'];
    container!.append(buildFilterUI());
    var monthNo = 1;
    for (var entry in sorted(months.keys.toList())) {
      container!.append(monthToHTML(entry));
      if (monthLimit != -1 && ++monthNo > monthLimit) {
        break;
      }
    }
  }

  /// Builds our UI
  Element build() {
    addFirefoxHack(querySelector('head')!);
    rebuild();
    return container!;
  }
}

Element fatalErrorMessage(dynamic e) {
  Element message = Element.div()
    ..setInnerHtml(
        "Migraine Log encountered a fatal error while building the interface.<br />Your data is still stored in the file, but Migraine Log is not able to display it here (you can, however, import it into the app).&nbsp;")
    ..append(Element.a()
      ..setAttribute(
          'href', "https://gitlab.com/mglog/org.zerodogg.migraineLog/issues")
      ..innerText = 'Please report this bug.');
  Element error = Element.div()
    ..append(message)
    ..append(Element.span()..innerText = 'Error code: ')
    ..append(Element.tag('code')..innerText = e.toString());
  if (debugHint != null) {
    error.append(Element.div()..innerText = "Debug hint: $debugHint");
  }
  return error;
}

void main() {
  // The loading element
  var loadingElement = querySelector('#loading')!;

  try {
    // The JSON data is in an element with the ID migraineLogData
    var jsonElement = querySelector('#migraineLogData')!;

    // Decode the data
    var data = jsonDecode(jsonElement.innerText);

    if (data["exportVersion"] != DATAVERSION) {
      debugHint = "exportVersion " +
          data["exportVersion"].toString() +
          " and DATAVERSION " +
          DATAVERSION.toString() +
          " do not match";
    }

    // Perform the UI build
    var dh = dataToHTML(data: data);

    // Replace the loading element with the data we just built
    loadingElement.replaceWith(dh.build());
  } catch (e) {
    loadingElement.replaceWith(fatalErrorMessage(e));
  }
}
