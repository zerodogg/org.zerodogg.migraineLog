// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'datatypes.dart';
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/foundation.dart'; // Needed for @required
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'i18n.dart';
import 'localehack.dart';
import 'definitions.dart';

class MigraineLogExporter extends MigraineListToHTML {
  // ignore: use_super_parameters
  MigraineLogExporter(MigraineList list,
      {required super.state, required super.config})
      : super(list);

  Future<void> export() async {
    if (defaultTargetPlatform == TargetPlatform.linux) {
      return linuxExporter();
    } else {
      return mobileExporter();
    }
  }

  Future<void> mobileExporter() async {
    final directory = await getApplicationDocumentsDirectory();
    File file = File(directory.path + '/migrainelog.html');
    await writeHTMLToFile(file);
    XFile xfile = XFile(file.path);
    unawaited(Share.shareXFiles([xfile]));
  }

  Future<void> linuxExporter() async {
    String? result = await FilePicker.platform.saveFile(
        fileName: "migrainelog.html",
        type: FileType.custom,
        allowedExtensions: ["html"]);
    if (result == null) {
      return;
    }
    await writeHTMLToFile(File(result));
  }
}

class MigraineListToExternalJSON {
  MigraineListToExternalJSON(
    this.list, {
    required this.state,
    required this.config,
  });

  final format = DateFormat(DateRenderFormat, LocaleHack.dateLocale);
  // This needs to use LLLL, rather than MMMM, to ensure proper formatting for
  // languages, like Finnish, that have several forms for month names. LLLL
  // will be correct for "MONTH YEAR", while MMMM is fine for proper full
  // dates.
  final monthFormat = DateFormat('LLLL yyyy', LocaleHack.dateLocale);

  String _noneMessage() {
    return Intl.message('(none)',
        desc:
            "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'");
  }

  String _takenMedsMessage() {
    return Intl.message("Medications",
        desc: "Used as a table header in exported data");
  }

  String _migraineLog() {
    return Intl.message("Migraine Log");
  }

  String _dateMessage() {
    return Intl.message('Date');
  }

  String _strengthMessage() {
    return Intl.message('Strength');
  }

  String _noteMessage() {
    return Intl.message('Note');
  }

  String _hideMessage() {
    return Intl.message('hide',
        desc:
            "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to");
  }

  String _hideTooltip() {
    return Intl.message('Hide this month',
        desc:
            'Tooltip for a link that hides a month from view in the exported HTML');
  }

  String _hiddenMessage() {
    return Intl.message('hidden:',
        desc: 'Will be rendered like: "hidden: january 2021"');
  }

  String totalHeadacheDays() {
    return Intl.message("Total headache days",
        desc:
            "Will be rendered in a table like: 'Total headache days: 20 days'");
  }

  String takenMedication() {
    return Intl.message("Taken medication");
  }

  String filterString() {
    return Intl.message("Filter:",
        desc:
            "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]");
  }

  String showString() {
    return Intl.message("show",
        desc:
            "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]");
  }

  String everythingString() {
    return Intl.message("everything",
        desc:
            "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]");
  }

  String monthsString() {
    return Intl.message("months",
        desc:
            "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'");
  }

  String listEmptyDaysString() {
    return Intl.message("List days with no entries",
        desc:
            "Checkbox, toggles showing or hiding (default=hide) dates that have no registered headaches in the tables in our exported HTML file");
  }

  // This is a terrible hack, but it works. The problem it attempts to solve is
  // the fact that our dart web component (web.dart) doesn't include any of the
  // i18n dart libraries, since including those would make the whole thing
  // balloon in size, over what we can tolerate for this kind of exported file.
  // So, since it doesn't include the i18n bits, it also can't generate
  // localized dates. There are two solutions to this, one is to jsObject proxy
  // to the Date object in JS and querying that. That's quite ugly, and could
  // lead to inconsistencies when the exported data gets rendered. The other
  // solution is to embed translations like we do for all of the other strings.
  // This is an implementation of the latter.
  Map dateStringsI18nListHack() {
    List weekdays = [];
    List months = [];
    var curr = DateTime(2021, 4, 5);
    var weekdayExtractor = DateFormat('EEEE', LocaleHack.dateLocale);
    var monthExtractor = DateFormat('MMMM', LocaleHack.dateLocale);
    while (curr.day <= 11) {
      weekdays.add(weekdayExtractor.format(curr));
      curr = curr.add(Duration(days: 1));
    }
    var month = 0;
    while (++month <= 12) {
      var date = DateTime(2021, month, 1);
      months.add(monthExtractor.format(date));
    }
    return {"weekdays": weekdays, "months": months};
  }

  MigraineList list;
  MigraineLogGlobalState state;
  MigraineLogConfig config;

  @visibleForTesting
  Map getMonthStats(DateTime entryDate) {
    var stats = list.headacheStats((DateTime dt) {
      return (dt.year == entryDate.year && dt.month == entryDate.month);
    }, DateTime(entryDate.year, entryDate.month, 1));
    var specificSummaries = {};
    for (MigraineLogStatisticsEntry entry in stats.entries) {
      if (entry.strength != null) {
        specificSummaries[
                config.messages.messageFromStrength(entry.strength!)] =
            MLi18nStrings.dayString(entry.days);
      }
    }
    // Add the base summaries
    return {
      "headacheDays": stats.headacheDays > 0
          ? MLi18nStrings.dayString(stats.headacheDays)
          : "0",
      "medicationDays": stats.medicationDays > 0
          ? MLi18nStrings.dayString(stats.medicationDays)
          : "0",
      "specificSummaries": specificSummaries,
    };
  }

  String getJSON() {
    Map exportData = {
      "exportVersion": DATAVERSION,
      "version": state.version,
      "data": {},
      "summaries": {},
      "strings": {
        "messages": {
          "migraineLog": _migraineLog(),
          "note": _noteMessage(),
          "none": _noneMessage(),
          "strength": _strengthMessage(),
          "date": _dateMessage(),
          "takenMeds": _takenMedsMessage(),
          "totalHeadacheDays": totalHeadacheDays(),
          "hide": _hideMessage(),
          "hideTooltip": _hideTooltip(),
          "hidden": _hiddenMessage(),
          "months": monthsString(),
          "everything": everythingString(),
          "show": showString(),
          "filter": filterString(),
          "listEmptyDays": listEmptyDaysString(),
          "dateStrings": dateStringsI18nListHack(),
        },
        "strength": {
          MigraineStrength.migraine.humanNumeric.toString():
              config.messages.migraineMesssage(),
          MigraineStrength.strongMigraine.humanNumeric.toString():
              config.messages.strongMigraineMessage(),
          MigraineStrength.headache.humanNumeric.toString():
              config.messages.headacheMessage(),
          MigraineStrength.auraOnly.humanNumeric.toString():
              config.messages.auraOnlyMessage(),
          MigraineStrength.noHeadache.humanNumeric.toString():
              config.messages.noHeadacheMessage(),
        },
        "months": {}
      }
    };

    // Loop through all entries in the MigraineList
    for (String entryDateStr in list.dates()) {
      DateTime entryDate = DateTime.parse(entryDateStr);
      MigraineEntry entry = list.get(entryDate)!;
      String subEntry = entryDate.year.toString() +
          entryDate.month.toString().padLeft(2, '0');
      // Generate summaries if we haven't already
      if (exportData["summaries"][subEntry] == null) {
        // Builds a list of stats for this month
        exportData["summaries"][subEntry] = getMonthStats(entryDate);
      }
      exportData["data"][subEntry] ??= {};
      exportData["data"][subEntry][entry.dateString] =
          entry.toMap(includeTimeString: true);
      exportData["data"][subEntry][entry.dateString]["date"] =
          format.format(entry.date);
      exportData["strings"]["months"][subEntry] = monthFormat.format(entryDate);
    }
    return jsonEncode(exportData);
  }
}

class MigraineListToHTML extends MigraineListToExternalJSON {
  HtmlEscape escape = HtmlEscape();

  // ignore: use_super_parameters
  MigraineListToHTML(
    list, {
    required super.state,
    required super.config,
  }) : super(list);

  String _generatedMessage(String version) {
    return Intl.message('Generated by Migraine Log version $version',
        args: [version], name: '_generatedMessage');
  }

  String loadingMessage() {
    return Intl.message("Loading...",
        desc:
            "Used in exported HTML to indicate that we're loading the exported data");
  }

  @visibleForTesting
  Future<String> asset() async {
    return await rootBundle.loadString('assets/web.js');
  }

  Future<String> get() async {
    String js = await asset();
    return '<!DOCTYPE html>'
            '<html><head><meta charset="utf-8" />'
            '<style type="text/css">'
            'html,body { margin:0px; }'
            'body { margin:0px; padding: 1em; }'
            'table { margin-bottom:1em; }'
            'body,.table { min-width: 700px; }'
            'table {border-collapse: collapse;}'
            'thead {display: table-header-group;}'
            'table, th, td {border: 1px solid black; padding: 5px;}'
            'footer { font-size: 0.8em; padding-top:2em; }'
            'a { text-decoration: none; }'
            'a:hover { text-decoration: underline; }'
            '#loading { min-height: 4em; }'
            '.ml-1 { margin-left: 1em; }'
            '.hide-month { margin-left: 1em; font-size: 50%; vertical-align: top; font-weight: normal; }'
            '.hide-month:hover,.hidden-month:hover,th:hover { text-decoration: underline; cursor:pointer; }'
            '.hidden-month { font-size: 75% }'
            '@media print { .filter, .hidden-month, .hide-month, .sortArrow { display: none; } }'
            '</style><meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no" />'
            '<title>' +
        escape.convert(_migraineLog()) +
        '</title>'
            '</head><body>' +
        '<div id="loading">' +
        escape.convert(loadingMessage()) +
        '</div>' +
        '<footer><a href="' +
        WebsiteURL.toString() +
        '">' +
        escape.convert(_generatedMessage(state.version!)) +
        '</a></footer>'
            '\n<script type="text/json" id="migraineLogData">\n' +
        getJSON() +
        '\n</script>\n' +
        '<!-- The following is the rendering code for the above JSON.\n'
            'This is built from the MigraineLog source code, specifically version ' +
        state.version! +
        '.\n'
            'Migraine Log is free software, and is licensed under the GNU General Public License\n'
            'version 3, see ' +
        WebsiteURL.toString() +
        ' for details and source code\n'
            'The following code also contains components generated from the dart SDK\n'
            'which is licensed under a BSD license. -->\n'
            '<script type="text/javascript">' +
        js +
        '</script>'
            '</body></html>';
  }

  Future<bool> writeHTMLToFile(File file) async {
    await file.writeAsString(await get());
    return true;
  }
}
