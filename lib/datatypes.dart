// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021-2025   Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'dart:io';
import 'definitions.dart';
import 'platforms.dart';
import 'localehack.dart';

/// This file defines all the internal datatypes used by migraine log

/// Data format upgrader
/// This upgrades from an older ON DISK storage format to the latest one
class MigraineLogDataUpgrader {
  MigraineLogDataUpgrader({required this.fromVersion});
  int fromVersion;

  void upgradeFullDataset(Map data) {
    for (String key in data.keys) {
      final entry = data[key];
      upgradeEntry(entry);
    }
  }

  Map upgradeEntry(Map entry) {
    if (fromVersion >= 1 && fromVersion <= 3) {
      _upgradeToTimedMedicationLists(entry);
    }
    return entry;
  }

  void _upgradeToTimedMedicationLists(entry) {
    if (entry["medicationList"] is List) {
      List<Map> newMedList = [];
      for (dynamic entry in entry["medicationList"]) {
        if (entry is String) {
          newMedList.add({
            "name": entry,
            "hour": -1,
            "minute": -1,
          });
        } else if (entry is Map) {
          newMedList.add(entry);
        }
      }
      entry["medicationList"] = newMedList;
    }
  }
}

/// An entry in the diary
class MigraineEntry extends ChangeNotifier with StringifyDate {
  MigraineStrength? _strength;
  DateTime _date = DateTime.now();
  String _note = '';
  MigraineMedicationList medications = MigraineMedicationList();
  bool _isClone = false;
  bool _isNewEntry = false;

  bool get isClone => _isClone;
  bool get isNewEntry => _isNewEntry;

  /// The list ("diary") this entry is a part of
  MigraineList? parentList;

  /// Builds a new, empty, MigraineEntry
  MigraineEntry({
    this.parentList,
    DateTime? date,
  }) {
    _isNewEntry = true;
    if (date != null) {
      _date = date;
    }
    medications.parent = this;
  }

  /// Function that wraps notifyListeners, in case we need to do more at some
  /// point
  void didChange() {
    notifyListeners();
  }

  /// The strength of the attack
  MigraineStrength? get strength => _strength;
  set strength(MigraineStrength? strength) {
    _strength = strength;
    didChange();
  }

  /// The date of the attack
  DateTime get date => _date;
  set date(DateTime date) {
    _date = date;
    didChange();
  }

  /// The date of the attack as a string
  String get dateString => dateTimeToISODate(date);

  /// Boolean, true if a note is present on this entry
  bool get hasNote {
    return note != "";
  }

  /// The users note. This can be both null and an empty string.
  String get note => _note;
  set note(String note) {
    _note = note;
    didChange();
  }

  /// Save ourself to parentList
  void save() {
    if (parentList != null) {
      parentList!.set(this);
    }
  }

  /// Get a clone of this entry
  MigraineEntry clone() {
    return MigraineEntry.fromMap(toMap(),
        parentList: parentList, date: _date, isAClone: true);
  }

  /// Get a map that can be used to store this entry
  Map toMap({bool includeTimeString = false}) {
    return {
      "strength": _strength?.humanNumeric,
      "note": _note,
      "medicationList":
          medications.toRawList(includeTimeString: includeTimeString)
    };
  }

  /// Build a new MigraineEntry from a map. Expects the map
  /// to match the map returned by toMap().
  MigraineEntry.fromMap(
    Map map, {
    DateTime? date,
    MigraineList? parentList,
    bool isAClone = false,
  }) {
    _isClone = isAClone;
    var strength = map['strength'];
    List? medicationList = map['medicationList'];
    if (strength != null) {
      var convertedStrength = MigraineStrengthConverter.fromNumber(strength);
      if (convertedStrength != null) {
        _strength = convertedStrength;
      }
    }
    if (date != null) {
      _date = date;
    }
    if (map['note'] != null) {
      _note = map['note'];
    }
    if (medicationList != null) {
      medications.setFromRawList(medicationList);
    }
    if (parentList != null) {
      this.parentList = parentList;
    }
    medications.parent = this;
  }
}

class MigraineMedicationEntry {
  MigraineMedicationEntry(
      {required this.medication,
      required int hour,
      required int minute,
      MigraineMedicationList? parent,
      this.iter = 0})
      : _hour = hour,
        _minute = minute,
        _parent = parent;
  int iter;
  String medication;
  int _hour = -1;
  int _minute = -1;
  dynamic _parent;

  int get hour => _hour;
  int get minute => _minute;

  int get timeInMinutes => _timeInMinutes();

  int _timeInMinutes() {
    int hour = _hour > 0 ? _hour : 0;
    int minute = _minute > 0 ? _minute : 0;
    return (hour * 60) + minute;
  }

  set parent(dynamic parent) {
    if (parent is MigraineMedicationList) {
      _parent = parent;
    } else {
      throw (ArgumentError(
          "Tried to set parent of MigraineMedicationEntry to unknown type: " +
              parent.runtimeType.toString()));
    }
  }

  void setTime(int hour, int minute) {
    _hour = hour;
    _minute = minute;
    didChange();
  }

  Key? _key;
  Key get key {
    _key ??= Key(
      iter.toString() +
          '//' +
          medication +
          '//' +
          _hour.toString() +
          '//' +
          _minute.toString() +
          DateTime.now().toString(),
    );
    return _key!;
  }

  Map toMap({bool includeTimeString = false}) {
    Map<String, dynamic> result = {
      "name": medication,
      "hour": hour,
      "minute": minute
    };
    if (includeTimeString) {
      result['timeString'] = timeStr();
    }
    return result;
  }

  MigraineMedicationEntry clone() {
    return MigraineMedicationEntry(
        medication: medication, hour: hour, minute: minute);
  }

  void _notifyParent() {
    if (_parent != null) {
      _parent.didChange();
    }
  }

  void didChange() {
    _notifyParent();
  }

  String timeStr() {
    if (hour == -1) {
      return '-';
    }
    // The date is irrelevant, so we just use a static one
    final DateTime time = DateTime(2025, 1, 1, hour, minute);
    return DateFormat.jm(LocaleHack.dateLocale).format(time);
  }
}

class MigraineMedicationList {
  dynamic _parent;
  List<MigraineMedicationEntry> _entries = [];

  int _iter = 0;

  /// Returns true if there is at least one entry in the list
  bool get isNotEmpty => _entries.isNotEmpty;

  // Worker that converts a dynamic into a MigraineMedicationEntry, or throws
  MigraineMedicationEntry _getMedicationEntry(
      dynamic medication, String errorMessagePrefix) {
    if (medication is MigraineMedicationEntry) {
      return medication;
    } else if (medication is Map) {
      return MigraineMedicationEntry(
          medication: medication['name'],
          hour: medication['hour'],
          minute: medication['minute'],
          iter: _iter++,
          parent: this);
    } else if (medication is String) {
      final now = DateTime.now();
      return MigraineMedicationEntry(
          medication: medication,
          hour: now.hour,
          minute: now.minute,
          iter: _iter++,
          parent: this);
    } else {
      throw (errorMessagePrefix +
          " got unknown type: " +
          medication.runtimeType.toString());
    }
  }

  void _notifyParent() {
    if (parent != null) {
      parent.didChange();
    }
  }

  void didChange() {
    _notifyParent();
  }

  set parent(dynamic parent) {
    if (parent is MigraineEntry || parent is MigraineLogConfig) {
      _parent = parent;
    } else {
      throw (ArgumentError(
          "Tried to set parent of MigraineMedicationList to unknown type: " +
              parent.runtimeType.toString()));
    }
  }

  /// Retrieve the parent of this list
  dynamic get parent => _parent;

  /// Get the number of entries in this list
  int get length => _entries.length;

  /// Retrieve the list of MigraineMedicationEntry contained within this
  /// MigraineMedicationList
  List<MigraineMedicationEntry> get list => _sortedEntries();

  /// Sort _entries and return it
  List<MigraineMedicationEntry> _sortedEntries() {
    _entries.sort((a, b) => a.timeInMinutes.compareTo(b.timeInMinutes));
    return _entries;
  }

  /// Add a medication to the list. medication can be either
  /// MigraineMedicationEntry or a String. Any other type will throw.
  void add(dynamic medication, {int? position}) {
    MigraineMedicationEntry entry =
        _getMedicationEntry(medication, "MigraineMedicationList.add()");
    entry.parent = this;
    if (position == null) {
      _entries.add(entry);
    } else {
      _entries.insert(position, entry);
    }
    _notifyParent();
  }

  /// Adds a list of medications to the list
  void addList(List medications) {
    for (var med in medications) {
      add(med);
    }
  }

  /// Sets a list of medications to this list. This will remove any entries
  /// already in the list, and replace them with the provided list. Expects the
  /// map to match the map returned by toRawList(). This may or may not include
  /// "timeString", which in either case is just ignored.
  void setFromRawList(List medications) {
    _entries = [];
    for (var entry in medications) {
      if (entry is String) {
        add(entry);
      } else if (entry is Map) {
        int hour = -1;
        int minute = -1;
        String medication = '';
        if (entry['hour'] is int) {
          hour = entry['hour'];
        }
        if (entry['minute'] is int) {
          minute = entry['minute'];
        }
        if (entry['name'] is String) {
          medication = entry['name'];
        }
        add(MigraineMedicationEntry(
            medication: medication, hour: hour, minute: minute));
      }
    }
  }

  /// Checks if a medication is present in this list
  bool has(MigraineMedicationEntry entry) {
    return _entries.indexWhere((testEntry) => testEntry.key == entry.key) != -1;
  }

  /// Checks if we have one or more entries named "name". Only useful for
  /// testing.
  @visibleForTesting
  bool hasEntryNamed(String name) {
    return _entries.indexWhere((testEntry) => testEntry.medication == name) !=
        -1;
  }

  /// Remove a medication from the list. Returns true if something was removed.
  bool remove(MigraineMedicationEntry medication) {
    int items = _entries.length;
    // Remove it
    _entries.removeWhere((testEntry) => testEntry.key == medication.key);
    _notifyParent();
    // Return it
    return items != _entries.length;
  }

  /// Searches through the list and removes empty entries
  void removeEmpty() {
    _entries.removeWhere((testEntry) => testEntry.medication == '');
    _notifyParent();
  }

  /// Has the user taken any medication?
  bool get takenMeds {
    return _entries.isNotEmpty;
  }

  List<Map> toRawList({bool includeTimeString = false}) {
    List<Map> rawList = [];
    for (var entry in _entries) {
      rawList.add(entry.toMap(includeTimeString: includeTimeString));
    }
    return rawList;
  }
}

/// A list of migraine attacks
class MigraineList extends ChangeNotifier with DelayedDataWrite, SafeDataWrite {
  MigraineList({required this.config});

  MigraineLogConfig config;
  Map _entries = {};
  Map _constructed = {};
  @visibleForTesting
  bool dirty = false;
  // The internal data version. This should, in the future, be used to upgrade
  // data and to avoid loading data that's not of a supported version
  final _dataVersion = DATAVERSION;

  /// Returns true if there is at least one entry in the list
  bool get isNotEmpty => _entries.isNotEmpty;

  /// Receives a string that reprents a date
  @visibleForTesting
  String stringifyDT(DateTime dt) {
    return [
      dt.year,
      dt.month.toString().padLeft(2, '0'),
      dt.day.toString().padLeft(2, '0')
    ].join('-');
  }

  /// Get the entry at `when`, or null if it doesn't exist
  MigraineEntry? get(DateTime when) {
    var strDT = stringifyDT(when);
    if (_entries[strDT] == null) {
      return null;
    }
    Map? entryItem = _entries[strDT];
    MigraineEntry? entry;
    if (_constructed[strDT] != null) {
      entry = _constructed[strDT];
    } else {
      entry = MigraineEntry.fromMap(entryItem!, parentList: this, date: when);
      _constructed[strDT] = entry;
    }
    return entry;
  }

  /// Get the entry at `when`, or a new empty one if it doesn't exist
  MigraineEntry getOrNew(DateTime when) {
    var entry = get(when);
    if (entry == null) {
      newEntry(date: when);
    }
    return entry as MigraineEntry;
  }

  /// Check if there's an entry on the date referred to by `when`
  bool exists(DateTime when) {
    var strDT = stringifyDT(when);
    return _entries[strDT] != null;
  }

  /// Remove an entry at `when` if present
  void remove(DateTime date) {
    var strDT = stringifyDT(date);
    if (_entries[strDT] != null) {
      dirty = true;
      _entries.remove(strDT);
      _constructed.remove(strDT);
      notifyListeners();
    }
  }

  /// Get a list of all date *strings*
  List dates() {
    return _entries.keys.toList();
  }

  /// Set an entry in this list. If an entry already exists on the same date,
  /// then it is overwritten.
  void set(MigraineEntry entry) {
    var strDT = stringifyDT(entry.date);
    _entries[strDT] = entry.toMap();
    _constructed[strDT] = entry;
    entry.parentList = this;
    dirty = true;

    // Schedule a data write in three minutes. This is to make sure we store
    // our data at regular intervals while we're open. If we've already written
    // the data before those three minutes are up, then _dirty will be false
    // and this will simply end up as a no-op.
    queueWrite(delayMinutes: 3);
    notifyListeners();
  }

  /// Get the number of elements in this MigraineList
  int get entries {
    return _entries.keys.length;
  }

  /// Get the path to where we store our file
  Future<File> dataFile() async {
    final directory = await getApplicationStorageDirectory();
    return File(directory.path + '/list.json');
  }

  /// Load data if there is any
  Future<FileLoadStatus> loadData() async {
    final dataPath = await dataFile();
    // First attempt to load the actual file
    final loadStatus = await loadDataFromFile(dataPath);
    // If this file load succeeded, or we have a versionFailure (ie. data is
    // too new), then we consider this file to have been loaded correctly, so
    // we return that
    if (loadStatus == FileLoadStatus.success ||
        loadStatus == FileLoadStatus.versionFailure) {
      return loadStatus;
    }
    // At this point loading the main file failed. Either because it didn't
    // exist, or because the JSON in the file was unparsable. Thus we first
    // try the temporary file that we write new data to before moving it into
    // place to replace old data.
    final temporaryFileRestoration = temporaryFileFor(dataPath);
    final temporaryFileRestorationLoadStatus =
        await loadDataFromFile(temporaryFileRestoration);
    if (temporaryFileRestorationLoadStatus == FileLoadStatus.success ||
        temporaryFileRestorationLoadStatus == FileLoadStatus.versionFailure) {
      return temporaryFileRestorationLoadStatus;
    }
    // Loading the temporary file also failed, either there isn't one or it
    // contained invalid data. Our last attempt is to try to load the backup
    // file.
    final backupFileRestoration = backupFileFor(dataPath);
    final backupFileRestorationLoadStatus =
        await loadDataFromFile(backupFileRestoration);
    // If no data existed, then we assume that no data is present. In
    // which case the loadData request has actually been successfully
    // processed.
    if (loadStatus == FileLoadStatus.existanceFailure &&
        backupFileRestorationLoadStatus == FileLoadStatus.existanceFailure &&
        temporaryFileRestorationLoadStatus == FileLoadStatus.existanceFailure) {
      return FileLoadStatus.success;
    }
    // If the backup file was successfully loaded, then we return success.
    // Otherwise we return whatever the status was when attampting to load the
    // main file.
    if (backupFileRestorationLoadStatus == FileLoadStatus.success) {
      return FileLoadStatus.success;
    }
    return loadStatus;
  }

  /// Load data from a named file
  @visibleForTesting
  Future<FileLoadStatus> loadDataFromFile(File file) async {
    if (!await file.exists()) {
      return FileLoadStatus.existanceFailure;
    }
    String contents = await file.readAsString();
    return loadDataFromJSON(contents);
  }

  /// Load data from a JSON string
  @visibleForTesting
  FileLoadStatus loadDataFromJSON(String json) {
    Map? data;
    try {
      data = jsonDecode(json);
    } on FormatException {
      return FileLoadStatus.jsonFailure;
    }
    assert(data != null);
    // Current data version: just handle it directly
    if (data!['dataVersion'] == _dataVersion) {
      _entries = data['entries'];
      _constructed = {};
      return FileLoadStatus.success;
    }
    // If dataVersion is older, then performVersionUpgrades
    if (data['dataVersion'] < DATAVERSION) {
      performVersionUpgrades(data['dataVersion'], data['entries']);
      _entries = data['entries'];
      _constructed = {};
      return FileLoadStatus.success;
    }
    // Data is too *new* and we don't know how to downgrade, so fail
    return FileLoadStatus.versionFailure;
  }

  /// Retrieve all data in this list as a Map that is equivalent to the on-disk
  /// storage format.
  @visibleForTesting
  Map getRawData() {
    return ({"dataVersion": _dataVersion, "entries": _entries});
  }

  /// Save the data if it's dirty
  Future<bool> saveData() async {
    if (!dirty) {
      return false;
    }
    final file = await dataFile();
    String contents = jsonEncode(getRawData());
    await writeStringToFile(contents, file);
    dirty = false;
    return true;
  }

  /// Upgrade versions 1, 2 and 3
  void performVersionUpgrades(int fromVersion, Map data) {
    final upgrader = MigraineLogDataUpgrader(fromVersion: fromVersion);
    upgrader.upgradeFullDataset(data);
  }

  /// Wraps saveData for DelayedDataWrite
  @override
  Future<bool> write() async {
    await saveData();
    return true;
  }

  /// Retrieve an empty MigraineEntry at the date provided
  MigraineEntry newEntry({DateTime? date}) {
    MigraineEntry entry = MigraineEntry(parentList: this);
    if (date != null) {
      entry.date = date;
    }
    return entry;
  }

  // Message used when there are no headaches
  String _nothingRegisteredMessage() {
    return Intl.message("No registration",
        desc:
            "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.");
  }

  /// Generate a MigraineLogStatisticsList for a specified period of time,
  /// starting at the date `start` and lasting until the callback `filter`
  /// returns false.
  MigraineLogStatisticsList headacheStats(
    bool Function(DateTime) filter,
    DateTime start,
  ) {
    // The first date to include
    DateTime processDate = start;

    // The list we will return
    var list = MigraineLogStatisticsList();

    // Statistics for days without headaches
    var nothingRegistered =
        MigraineLogStatisticsEntry(title: _nothingRegisteredMessage());
    // Statistics for days with strong migraines
    var strongMigraine = MigraineLogStatisticsEntry(
      title: config.messages.strongMigraineMessage(),
      strength: MigraineStrength.strongMigraine,
    );
    // Statistics for days with migraines
    var migraine = MigraineLogStatisticsEntry(
      title: config.messages.migraineMesssage(),
      strength: MigraineStrength.migraine,
    );
    // Statistics for days with headaches
    var headache = MigraineLogStatisticsEntry(
      title: config.messages.headacheMessage(),
      strength: MigraineStrength.headache,
    );
    // Statistics for days with auras
    var auraOnly = MigraineLogStatisticsEntry(
      title: config.messages.auraOnlyMessage(),
      strength: MigraineStrength.auraOnly,
    );
    // Statistics for days with no headaches (notes only)
    var noHeadache = MigraineLogStatisticsEntry(
      title: config.messages.noHeadacheMessage(),
      strength: MigraineStrength.noHeadache,
    );

    int ofDays = 0;

    // Iterate through all days between processDate and now
    while (filter(processDate)) {
      ofDays++;
      MigraineEntry? entry = get(processDate);
      // If we got no entry, then there are no registration for this day
      if (entry == null) {
        nothingRegistered.days++;
      } else {
        // We will figure out which MigraineLogStatisticsEntry to store the data in
        late MigraineLogStatisticsEntry chosen;
        switch (entry.strength) {
          case MigraineStrength.migraine:
            {
              chosen = migraine;
            }
            break;

          case MigraineStrength.strongMigraine:
            {
              chosen = strongMigraine;
            }
            break;

          case MigraineStrength.headache:
            {
              chosen = headache;
            }
            break;
          case MigraineStrength.auraOnly:
            {
              chosen = auraOnly;
            }
            break;
          case MigraineStrength.noHeadache:
            {
              chosen = noHeadache;
            }
            break;
          // This is actually impossible
          case null:
            throw ("MigraineList.headacheStats: got a null strength, this should be impossible");
        }
        // Increase the number of days registered
        chosen.days++;
        // Increase the number of days with medications taken, if registered
        if (entry.medications.takenMeds) {
          list.medicationDays++;
        }
      }
      // Bump processDate
      var previousDate = processDate;
      processDate = processDate.add(Duration(days: 1));
      // DateTime and Duration doesn't really handle daylight savings changes
      // very well, so we could end up with adding Duration(days:1) (which is
      // the number of seconds in 24 hours), but DST having changed time one
      // hour ahead (and so we add 24 hours, but to switch days we needed 25
      // hours) we end up on the same day. This loop adds an hour each
      // iteration until we change days.
      while (processDate.day == previousDate.day) {
        processDate = processDate.add(Duration(hours: 1));
      }
    }
    // Add our entries to the list
    list.entries = [
      nothingRegistered,
      strongMigraine,
      migraine,
      headache,
      auraOnly,
      noHeadache,
    ];
    // Set the ofDays
    list.ofDays = ofDays;
    // Remove entries if we have any with days=0
    list.entries.removeWhere((entry) => entry.days == 0);

    return list;
  }

  /// Generate a MigraineLogStatisticsList for the last `forDays` days
  MigraineLogStatisticsList headacheDaysStats(int forDays) {
    DateTime now = DateTime.now();
    // The first date to include
    DateTime firstDate = now.subtract(Duration(days: forDays - 1));
    return headacheStats(
      (DateTime dt) {
        return (dt.isBefore(now) || dt == now);
      },
      firstDate,
    );
  }
}

/// A list of statistics for a time period
class MigraineLogStatisticsList {
  MigraineLogStatisticsList({int? ofDays}) {
    _ofDays = ofDays ?? 0;
  }
  List<MigraineLogStatisticsEntry> _entries = [];

  // Propagate our _ofDays value to all of our child entries
  void _propagateOfDays() {
    for (var entry in entries) {
      entry.ofDays = _ofDays;
    }
  }

  int _ofDays = 0;

  /// The number of days this MigraineLogStatisticsList contains. You can
  /// change this at any time (and the headacheStats method of MigraineList
  /// does just that). Changing this also updates ofDays in all of this lists
  /// entries.
  set ofDays(int ofD) {
    _ofDays = ofD;
    _propagateOfDays();
  }

  int get ofDays => _ofDays;

  // Retrieve the list of entries in this MigraineLogStatisticsList
  List<MigraineLogStatisticsEntry> get entries => _entries;

  /// Set the list of entries in this MigraineLogStatisticsList
  ///
  /// Setting the entries this way will also propagate ofDays to the children.
  /// If, instead of replacing the list, you append to it, then you will
  /// need to propagate ofDays manually.
  set entries(List<MigraineLogStatisticsEntry> entryList) {
    _entries = entryList;
    _propagateOfDays();
  }

  Iterable<MigraineLogStatisticsEntry> get headacheEntires =>
      _entries.where((e) => e.strength != null);

  /// The number of days in this time period that medications have been used
  int medicationDays = 0;

  /// Get the number of days in this MigraineLogStatisticsList that a user
  /// had headaches
  int get headacheDays {
    int days = 0;
    for (var type in entries) {
      /// XXX: Should MigraineStrength.auraOnly also be excluded?
      if (type.strength != null &&
          type.strength != MigraineStrength.noHeadache) {
        days += type.days;
      }
    }
    return days;
  }

  /// Get the percentage of days that have had headaches
  double get headachePercentage {
    return (headacheDays / _ofDays) * 100;
  }

  /// Get the percentage of days that the user used any medication
  double get medicationPercentage {
    return (medicationDays / _ofDays) * 100;
  }
}

/// A single entry of statistics
class MigraineLogStatisticsEntry {
  MigraineLogStatisticsEntry(
      {this.title, this.days = 0, this.strength, this.ofDays = 0});

  /// The number of days that the period that this entry contains statistics
  /// for. This is not the number of days that had events, that is represented
  /// as days.
  int ofDays;

  /// How strong a migraine this entry represents. This can be null for "no
  /// headache".
  MigraineStrength? strength;

  /// The title used to render this entry
  String? title;

  /// The number of days of attacks of type strength
  int days;

  /// The number of days with attacks of type strength where the user also used
  /// medications
  int medicationDays = 0;

  /// Retrieve the percentage of the total number of days in the given period
  /// that this entry contains
  double get percentageOfDays {
    if (days == 0 && ofDays == 0) {
      return 0;
    }
    return (days / ofDays) * 100;
  }
}

/// Our global state object
class MigraineLogGlobalState extends ChangeNotifier {
  DateTime _currentDate = DateTime.now();
  DateTime _currentCalendarMonth = DateTime.now();
  // Changes to this shouldn't trigger anything
  String? version;

  void didChange() {
    notifyListeners();
  }

  /// The current date selected in the global calendar
  DateTime get currentDate => _currentDate;

  set currentDate(DateTime date) {
    _currentDate = date;
    didChange();
  }

  /// The current date representing the viewed calendar month
  DateTime get currentCalendarMonth => _currentCalendarMonth;

  set currentCalendarMonth(DateTime date) {
    _currentCalendarMonth = date;
    didChange();
  }
}

// This class is only public so that it can be tested
@visibleForTesting
class MigraineLogConfigMessages {
  bool neutralLanguage = false;

  String _headacheMessage() {
    return Intl.message('Headache',
        desc: "Used as level 1/3 (lowest) headache strength");
  }

  String _headacheMessageNeutral() {
    return Intl.message('Mild headache',
        desc: "Used as neutral level 1/3 (lowest) headache strength");
  }

  String headacheMessage() {
    if (neutralLanguage) {
      return _headacheMessageNeutral();
    }
    return _headacheMessage();
  }

  String _migraineMessage() {
    return Intl.message('Migraine',
        desc: "Used as level 2/3 (medium) headache strength");
  }

  String _migraineMessageNeutral() {
    return Intl.message('Moderate headache',
        desc: "Used as neutral level 2/3 (medium) headache strength");
  }

  String migraineMesssage() {
    if (neutralLanguage) {
      return _migraineMessageNeutral();
    }
    return _migraineMessage();
  }

  String _strongMigraineMessage() {
    return Intl.message('Strong migraine',
        desc: "Used as level 3/3 (strongest) headache strength");
  }

  String _strongMigraineMessageNeutral() {
    return Intl.message('Strong headache',
        desc: "Used as neutral level 3/3 (strongest) headache strength");
  }

  String strongMigraineMessage() {
    if (neutralLanguage) {
      return _strongMigraineMessageNeutral();
    }
    return _strongMigraineMessage();
  }

  String auraOnlyMessage() {
    return Intl.message(
      "Aura only",
      desc:
          "Used as the 'strength' of a migraine or headache where the user only had aura symptoms",
    );
  }

  String noHeadacheMessage() {
    return Intl.message(
      "No headache",
      desc:
          "Used as the 'strength' of a day where the user had no headache (for registering notes on days without symptoms)",
    );
  }

  String messageFromStrength(MigraineStrength strength) {
    switch (strength) {
      case MigraineStrength.auraOnly:
        return auraOnlyMessage();
      case MigraineStrength.headache:
        return headacheMessage();
      case MigraineStrength.migraine:
        return migraineMesssage();
      case MigraineStrength.strongMigraine:
        return strongMigraineMessage();
      case MigraineStrength.noHeadache:
        return noHeadacheMessage();
    }
  }
}

class MigraineLogConfig extends ChangeNotifier
    with DelayedDataWrite, SafeDataWrite {
  MigraineLogConfig() {
    // Needed so that the list can notify us about changes
    medications.parent = this;
  }
  MigraineConfigMedicationList medications = MigraineConfigMedicationList();
  MigraineLogConfigMessages messages = MigraineLogConfigMessages();
  int _onboardingVersion = 0;
  bool _dirty = false;
  bool _neutralLanguage = false;
  bool _displayNoteMarker = false;
  bool _enableAuraStrength = false;
  bool _enableNoHeadacheStrength = false;

  bool get neutralLanguage => _neutralLanguage;
  set neutralLanguage(bool value) {
    _neutralLanguage = value;
    messages.neutralLanguage = value;
    didChange();
  }

  bool get displayNoteMarker => _displayNoteMarker;
  set displayNoteMarker(bool value) {
    _displayNoteMarker = value;
    didChange();
  }

  bool get enableAuraStrength => _enableAuraStrength;
  set enableAuraStrength(bool value) {
    _enableAuraStrength = value;
    didChange();
  }

  bool get enableNoHeadacheStrength => _enableNoHeadacheStrength;
  set enableNoHeadacheStrength(bool value) {
    _enableNoHeadacheStrength = value;
    didChange();
  }

  @visibleForTesting
  String migraineMedString() {
    return Intl.message("Migraine medication",
        desc:
            "Used as a generic 'medication' when the user opts not to add their own");
  }

  @visibleForTesting
  String genericSeizureMedString() {
    return Intl.message("Seizure medication",
        desc:
            "Used as neutral generic 'medication' when the user opts not to add their own");
  }

  @visibleForTesting
  String analgesicsMedString() {
    return Intl.message("Painkillers",
        desc:
            "Used as the second 'medication' when the user opts not to add their own");
  }

  int get onboardingVersion => _onboardingVersion;
  set onboardingVersion(int version) {
    _onboardingVersion = version;
    didChange();
  }

  void didChange() {
    notifyListeners();
    _dirty = true;
    queueWrite(delayMinutes: 1);
  }

  /// Get the path to where we store our config
  Future<File> configFilePath() async {
    final directory = await getApplicationStorageDirectory();
    return File(directory.path + '/config.json');
  }

  /// Get the path to either the main config, or a fallback config
  Future<File> anyConfigFilePath() async {
    final confPath = await configFilePath();
    return filePathOrFallback(confPath);
  }

  /// Load data if there is any
  Future<void> loadConfig() async {
    final file = await anyConfigFilePath();
    if (!await file.exists()) {
      // If no config exists just return
      return;
    }
    String contents = await file.readAsString();
    Map data = jsonDecode(contents);
    if (data['onboardingVersion'] != null && data['onboardingVersion'] is int) {
      _onboardingVersion = data['onboardingVersion'];
    }
    if (data['medication'] != null && data['medication'] is List) {
      medications.setList(data['medication']);
    }
    if (data['neutralLanguage'] != null && data['neutralLanguage'] is bool) {
      _neutralLanguage = data['neutralLanguage'];
      messages.neutralLanguage = data['neutralLanguage'];
    }
    if (data['displayNoteMarker'] != null &&
        data['displayNoteMarker'] is bool) {
      _displayNoteMarker = data['displayNoteMarker'];
    }
    if (data['enableAuraStrength'] != null &&
        data['enableAuraStrength'] is bool) {
      _enableAuraStrength = data['enableAuraStrength'];
    }
    if (data['enableNoHeadacheStrength'] != null &&
        data['enableNoHeadacheStrength'] is bool) {
      _enableNoHeadacheStrength = data['enableNoHeadacheStrength'];
    }
  }

  /// Save the config
  Future<bool> saveConfig() async {
    /// Don't write if there's nothing new
    if (!_dirty) {
      return false;
    }
    final file = await configFilePath();
    String contents = jsonEncode({
      "onboardingVersion": onboardingVersion,
      "medication": medications.toStrList(),
      "neutralLanguage": neutralLanguage,
      "displayNoteMarker": displayNoteMarker,
      "enableAuraStrength": enableAuraStrength,
      "enableNoHeadacheStrength": enableNoHeadacheStrength,
      "configVersion": 1
    });
    await writeStringToFile(contents, file);
    _dirty = false;
    return true;
  }

  void resetConfig() {
    _dirty = true;
    _neutralLanguage = false;
    _displayNoteMarker = false;
    _enableAuraStrength = false;
    _enableNoHeadacheStrength = false;
    _onboardingVersion = 0;
    medications = MigraineConfigMedicationList();
  }

  /// saveConfig wrapper for DelayedDataWrite
  @override
  void write() {
    saveConfig();
  }

  /// Finalizes onboarding to version
  void onboardingDone(int version) {
    if (medications.length == 0) {
      medications.setList([
        neutralLanguage ? genericSeizureMedString() : migraineMedString(),
        analgesicsMedString(),
      ]);
    }
    onboardingVersion = version;
  }
}

/// Parameters for the editor component
class MigraineEditorPathParameters {
  MigraineEditorPathParameters({this.date, this.entry});

  /// The date to create a new entry on or to edit
  DateTime? date;

  /// The entry, either a new one (for creation) or one that should be edited
  MigraineEntry? entry;
}

/// A mixin that implements delayed writes
mixin DelayedDataWrite {
  Future? _queuedWrite;

  /// Queues a write operation for an object to occur after the spcified delay.
  /// If a write has already been queued then we won't queue another.
  ///
  /// This merely wraps the consumer's write method. Any checks to see if the
  /// write is still needed should occur there.
  void queueWrite({int delayMinutes = 1}) {
    _queuedWrite ??= Future.delayed(Duration(minutes: delayMinutes), () {
      write();
      _queuedWrite = null;
    });
  }

  /// Our consumer must implement this
  void write() {
    throw ('Consumer of mixin must implement write()');
  }
}

/// A mixin that provides safer write logic
mixin SafeDataWrite {
  /// Safely writes data to writeToPath
  Future<void> writeStringToFile(String data, File writeToPath) async {
    if (writeToPath.existsSync()) {
      // Get a File object pointing to a temporary location
      var tmpFile = temporaryFileFor(writeToPath);
      // Write our data to the temporary file
      await tmpFile.writeAsString(data);
      // Move the old original file to file~
      writeToPath.renameSync(backupFileFor(writeToPath).path);
      // Move our renamed file into place, replacing the previously written file
      tmpFile.renameSync(writeToPath.path);
    } else {
      // There's no data that exists already, just write directly
      await writeToPath.writeAsString(data);
    }
  }

  /// If path exists, returns path, if it doesn't it checks to see if path~
  /// exists, if it does then that gets returned, otherwise it checks to see if
  /// path.new exists, if it does then that gets returned, finally if none
  /// exist then path is returned
  ///
  /// Use this to be able to fall back to the path~ backup file or .new
  /// temporary datafile, both of which gets created by writeStringToFile.
  File filePathOrFallback(File path) {
    if (path.existsSync()) {
      return path;
    }
    var fallback1 = backupFileFor(path);
    if (fallback1.existsSync()) {
      return fallback1;
    }
    var fallback2 = File(path.path + '.new');
    if (fallback2.existsSync()) {
      return fallback2;
    }
    return path;
  }

  /// Given a `File`, returns the temporary File that we should use when
  /// writing data to that file.
  File temporaryFileFor(File path) {
    return File(path.path + '.new');
  }

  /// Given a `File`, returns the backup File that the old contents of that file
  /// should be written to.
  File backupFileFor(File path) {
    return File(path.path + '~');
  }
}

enum FileLoadStatus { success, jsonFailure, versionFailure, existanceFailure }

mixin StringifyDate {
  String dateTimeToISODate(DateTime dt) {
    return [
      dt.year,
      dt.month.toString().padLeft(2, '0'),
      dt.day.toString().padLeft(2, '0')
    ].join('-');
  }
}

class MigraineConfigMedicationList {
  dynamic _parent;
  List<MigraineConfigMedicationEntry> _entries = [];

  MigraineConfigMedicationEntry? _emptyEntry;

  int _iter = 0;

  /// Returns true if there is at least one entry in the list
  bool get isNotEmpty => _entries.isNotEmpty;

  // Worker that converts a dynamic into a MigraineConfigMedicationEntry, or throws
  MigraineConfigMedicationEntry _getMedicationEntry(
      dynamic medication, String errorMessagePrefix) {
    if (medication is MigraineConfigMedicationEntry) {
      return medication;
    } else if (medication is String) {
      return MigraineConfigMedicationEntry(
          medication: medication, iter: _iter++);
    } else {
      throw (errorMessagePrefix +
          " got unknown type: " +
          medication.runtimeType.toString());
    }
  }

  void _notifyParent() {
    if (parent != null) {
      parent.didChange();
    }
  }

  set parent(dynamic parent) {
    if (parent is MigraineEntry || parent is MigraineLogConfig) {
      _parent = parent;
    } else {
      throw (ArgumentError(
          "Tried to set parent of MigraineMedicationList to unknown type: " +
              parent.runtimeType.toString()));
    }
  }

  /// Moves an item in the list, from index oldIndex to newIndex
  void reorderByIndex(int oldIndex, int newIndex) {
    // Retrieve the entry
    var element = _entries[oldIndex];
    if (newIndex >= _entries.length) {
      _entries.add(element);
      _entries.removeAt(oldIndex);
    } else if (oldIndex > newIndex) {
      _entries.insert(newIndex, element);
      _entries.removeAt(oldIndex + 1);
    } else {
      _entries.removeAt(oldIndex);
      _entries.insert(newIndex, element);
    }
    _notifyParent();
  }

  /// Replaces an existing entry in the list with a new entry
  void replace(dynamic existing, dynamic replacement) {
    var existingEntry = _getMedicationEntry(
        existing, 'existing in MigraineMedicationList.replace()');
    var replacementEntry = _getMedicationEntry(
        replacement, 'replacement in MigraineMedicationList.replace()');
    var idx = _entries
        .indexWhere((entry) => entry.medication == existingEntry.medication);
    _entries[idx] = replacementEntry;
  }

  /// Retrieve the parent of this list
  dynamic get parent => _parent;

  /// Get the number of entries in this list
  int get length => _entries.length;

  /// Retrieve the list of MigraineConfigMedicationEntry contained within this
  /// MigraineMedicationList
  List<MigraineConfigMedicationEntry> get list => _entries;

  /// Retrieve the list of MigraineConfigMedicationEntry contained within this
  /// MigraineMedicationList, along with a single empty MigraineConfigMedicationEntry
  /// at the end. The primary intention of this function is to be used to
  /// easily display an "Add a new medication" entry at the end of the
  /// medication list in the settings.
  ///
  /// This tries to be smart about the empty entry. It will reuse a single
  /// MigraineConfigMedicationEntry consisting of an empty string ("") for multiple calls,
  /// as long as that entry has not been modified since the last call. If the
  /// entry has been modified (ie. it's no longer empty), then a new
  /// MigraineConfigMedicationEntry will be built.
  List<MigraineConfigMedicationEntry /*!*/ > get listWithEmpty {
    List<MigraineConfigMedicationEntry> list =
        List<MigraineConfigMedicationEntry>.from(_entries);
    if (_emptyEntry == null || _emptyEntry!.medication != '') {
      _emptyEntry = _getMedicationEntry('', 'listWithEmpty()');
    }
    list.add(_emptyEntry!);
    return list;
  }

  /// Add a medication to the list. medication can be either
  /// MigraineConfigMedicationEntry or a String. Any other type will throw.
  void add(dynamic medication, {int? position}) {
    MigraineConfigMedicationEntry entry =
        _getMedicationEntry(medication, "MigraineMedicationList.add()");
    if (position == null) {
      _entries.add(entry);
    } else {
      _entries.insert(position, entry);
    }
    _notifyParent();
  }

  /// Adds a list of medications to the list
  void addList(List medications) {
    for (var med in medications) {
      add(med);
    }
  }

  /// Sets a list of medications to this list. This will remove any entries
  /// already in the list, and replace them with the provided list.
  void setList(List medications) {
    _entries = [];
    addList(medications);
  }

  /// Checks if a medication is present in this list
  bool has(MigraineConfigMedicationEntry entry) {
    return _entries.indexWhere(
            (testEntry) => testEntry.medication == entry.medication) !=
        -1;
  }

  /// Remove a medication from the list. medication can be either
  /// MigraineConfigMedicationEntry or a String. Any other type will throw.
  int remove(dynamic medication) {
    MigraineConfigMedicationEntry entry =
        _getMedicationEntry(medication, "MigraineMedicationList.add()");
    // Retrieve the index number of this entry
    int number = _entries
        .indexWhere((testEntry) => testEntry.medication == entry.medication);
    // Remove it
    _entries
        .removeWhere((testEntry) => testEntry.medication == entry.medication);
    _notifyParent();
    // Return it
    return number;
  }

  /// Searches through the list and removes empty entries
  void removeEmpty() {
    _entries.removeWhere((testEntry) => testEntry.medication == '');
    _notifyParent();
  }

  /// Converts a MigraineMedicationList into a List of Strings
  List<String> toStrList() {
    List<String> strEntries = [];
    for (var entry in _entries) {
      strEntries.add(entry.medication);
    }
    return strEntries;
  }
}

class MigraineConfigMedicationEntry {
  MigraineConfigMedicationEntry({required this.medication, this.iter = 0});
  int iter;
  String medication;
  Key? _key;
  Key get key {
    _key ??= Key(
      iter.toString() + '//' + medication + '//' + DateTime.now().toString(),
    );
    return _key!;
  }

  MigraineConfigMedicationEntry clone() {
    return MigraineConfigMedicationEntry(medication: medication);
  }
}
